package com.marco.list.linkedList;

import java.util.*;

class LinkedList<E> implements List<E>, Deque<E> {
    private Node<E> start;
    private Node<E> end;
    private int size;

    public LinkedList() {
        this.start = this.end = null;
        this.size = 0;
    }

    @Override
    public void addFirst(E e) {
        Node<E> nodeToInsert = new Node<>(e, null);
        if (start == null) {
            start = end = nodeToInsert;
        } else {
            nodeToInsert.setNext(start);
            start = nodeToInsert;
        }
        size++;
    }

    @Override
    public void addLast(E e) {
        Node<E> nodeToInsert = new Node<>(e, null);
        if (start == null) {
            start = end = nodeToInsert;
        } else {
            end.setNext(nodeToInsert);
            end = nodeToInsert;
        }
        size++;
    }

    @Override
    public boolean offerFirst(E e) {
        addFirst(e);
        return true;
    }

    @Override
    public boolean offerLast(E e) {
        addLast(e);
        return true;
    }

    @Override
    public E removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        E toReturn = getFirst();
        start = start.getNext();
        size--;
        return toReturn;
    }

    @Override
    public E removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        E toReturn = end.getElement();
        end = getNode(size - 2);
        size--;
        return toReturn;
    }

    @Override
    public E pollFirst() {
        if (isEmpty()) {
            return null;
        }
        return removeFirst();
    }

    @Override
    public E pollLast() {
        if (isEmpty()) {
            return null;
        }
        return removeLast();
    }

    @Override
    public E getFirst() {
        if (start == null) {
            throw new NoSuchElementException();
        }
        return start.getElement();
    }

    @Override
    public E getLast() {
        if (end == null) {
            throw new NoSuchElementException();
        }
        return end.getElement();
    }

    @Override
    public E peekFirst() {
        return getFirst();
    }

    @Override
    public E peekLast() {
        return getLast();
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        int oIndex = indexOf(o);
        if (oIndex > -1) {
            remove(oIndex);
            return true;
        }
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        int oIndex = lastIndexOf(o);
        if (oIndex > -1) {
            remove(oIndex);
            return true;
        }
        return false;
    }

    @Override
    public boolean offer(E e) {
        return add(e);
    }

    @Override
    public E remove() {
        return removeFirst();
    }

    @Override
    public E poll() {
        return pollFirst();
    }

    @Override
    public E element() {
        if (start == null) {
            throw new NoSuchElementException();
        }
        return start.getElement();
    }

    @Override
    public E peek() {
        return element();
    }

    @Override
    public void push(E e) {
        addFirst(e);
    }

    @Override
    public E pop() {
        return removeFirst();
    }

    @Override
    public Iterator<E> descendingIterator() {
        return null;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return start == null;
    }

    @Override
    public boolean contains(Object o) {
        Node<E> current = start;
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (current.getElement() == null) {
                    return true;
                }
                current = current.getNext();
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (current.getElement().equals(o)) {
                    return true;
                }
                current = current.getNext();
            }
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIter<>();
    }

    private class LinkedListIter<E> implements Iterator<E> {
        Node<E> current;

        LinkedListIter() {
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public E next() {
            E data = current.getElement();
            current = current.getNext();
            return data;
        }
    }

    @Override
    public Object[] toArray() {
        E[] toReturn = (E[]) new Object[size];
        Node<E> current = start;
        for (int i = 0; i < size; i++) {
            toReturn[i] = current.getElement();
            current = current.getNext();
        }
        return toReturn;
    }

    @Override
    public <T> T[] toArray(T[] a) { // todo: all
        return null;
    }

    @Override
    public boolean add(E e) { // O(1)
        addLast(e);
        return (get(size - 1) == e);
    }

    @Override
    public boolean remove(Object o) {
        Node<E> current = start;
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (current.getElement() == null) {
                    remove(i);
                    return true;
                }
                current = current.getNext();
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (current.getElement().equals(o)) {
                    remove(i);
                    return true;
                }
                current = current.getNext();
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        if (Objects.isNull(c)) {
            throw new NullPointerException();
        }
        for (E item : c) {
            add(item);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        isIndexOutOfBounds(index);
        if (Objects.isNull(c)) {
            throw new NullPointerException();
        }
        for (E item : c) {
            add(index, item);
            index++;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) { // todo: all

        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) { // todo: all
        return false;
    }

    @Override
    public void clear() {
        start = null;
        end = null;
        size = 0;
    }

    public Node<E> getNode(int index) {
        isIndexOutOfBounds(index);

        Node node = start;
        for (int i = 0; node != null && i < index; i++) {
            node = node.getNext();
        }
        return node;
    }

    @Override
    public E get(int index) { // O(n)
        return getNode(index).getElement();
    }

    private void isIndexOutOfBounds(int index) {
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public E set(int index, E element) {
        isIndexOutOfBounds(index);

        Node<E> node = getNode(index);
        E toReturn = node.getElement();
        node.setElement(element);
        return toReturn;
    }

    @Override
    public void add(int index, E element) {
        isIndexOutOfBounds(index);

        if (index == 0) {
            addFirst(element);
            return;
        }
        if (index == size) {
            addLast(element);
            return;
        }

        Node<E> nodeToBeInserted = new Node<>(element, null);
        Node<E> current = start;

        for (int i = 1; i < index; i++) {
            current = current.getNext();
        }
        nodeToBeInserted.setNext(current.getNext());
        current.setNext(nodeToBeInserted);
        size++;
    }

    @Override
    public E remove(int index) { // O(n)
        isIndexOutOfBounds(index);

        if (index == 0) {
            Node<E> temp = start;
            start = start.getNext();
            return temp.getElement();
        }
        E temp = get(index);
        Node<E> parent = getNode(index - 1);
        parent.setNext(parent.getNext().getNext());
        this.size--;
        return temp;
    }

    @Override
    public int indexOf(Object o) {
        Node<E> current = start;
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (current.getElement() == null) {
                    return i;
                }
                current = current.getNext();
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(current.getElement())) {
                    return i;
                }
                current = current.getNext();
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<E> current = start;
        int foundAt = -1;
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (current.getElement() == null) {
                    foundAt = i;
                }
                current = current.getNext();
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(current.getElement())) {
                    foundAt = i;
                }
                current = current.getNext();
            }
        }
        return foundAt;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) { // todo: all
        return null;
    }

    // Floyd's loop detection algorithm
    public boolean checkLoop() {
        Node slow = start;
        Node fast = start;

        while (fast != null && fast.getNext() != null && fast.getNext().getNext() != null) {
            slow = slow.getNext();
            fast = fast.getNext().getNext();
            if (slow == fast) {
                return true;
            }
        }

        return false;
    }
}
