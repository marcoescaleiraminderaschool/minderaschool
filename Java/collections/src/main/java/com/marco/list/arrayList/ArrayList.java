package com.marco.list.arrayList;

import java.util.*;

import static java.util.stream.Collectors.toList;

public class ArrayList<E> implements List<E> {
    private static final int DEFAULT_CAPACITY = 10;
    private int size = 0;
    private E[] elements;

    @SuppressWarnings("unchecked")
    public ArrayList() { //Default constructor
        elements = (E[]) new Object[DEFAULT_CAPACITY];
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean contains(Object o) {
        if (o == null) {
            for (int i = 0; i < elements.length; i++) {
                if (elements[i] == null) {
                    return true;
                }
            }
        } else {
            for (E element : elements) {
                if (o.equals(element)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Iterator iterator() {
        return new ArrListIter<E>();
    }

    private class ArrListIter<E> implements Iterator<E> {
        int currentPos;
        ArrListIter() {
            currentPos = 0;
        }
        @Override
        public boolean hasNext() {
            return currentPos < size;
        }
        @Override
        public E next() {
            return (E) ArrayList.this.elements[currentPos++];
        }
    }

    @SuppressWarnings("unchecked")
    public E remove(int index) {
        isOutOfBoundsWithLength(index);

        E removed = elements[index];
        E[] temp = (E[]) new Object[elements.length];
        for (int i = 0, j = 0; i < elements.length; i++) {
            if (i != index) {
                temp[j++] = elements[i];
            }
        }
        size--;
        elements = temp;
        return removed;
    }

    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(get(i))) {
                int j = i;
                for (; j < size; j++) {
                    elements[j] = elements[j + 1];
                }
                elements[j + 1] = null;
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        if (c == null || c.contains(null)) {
            throw new NullPointerException();
        }
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    public boolean add(E o) {
        if (size == elements.length) {
            duplicateCapacity();
        }
        elements[size++] = o;
        return true;
    }

    public void add(int index, E element) {
        isIndexOutOfBounds(index);

        if (size + 1 == elements.length) {
            duplicateCapacity();
        }
        for (int i = size; i >= index; i--) {
            elements[i + 1] = elements[i];
        }
        elements[index] = element;
        size++;
    }

    private void isIndexOutOfBounds(int index) {
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException();
        }
    }

    public boolean addAll(Collection<? extends E> c) {
        if (c == null) {
            throw new NullPointerException();
        }
        for (E element : c) {
            add(element);
        }
        return true;
    }

    public boolean addAll(int index, Collection c) {
        isIndexOutOfBounds(index);
        if (c == null) {
            throw new NullPointerException();
        }
        for(Object o : c){
            add(index, (E) o);
            index++;
        }
        return true;
    }

    private void duplicateCapacity() {
        elements = Arrays.copyOf(elements, elements.length * 2);
    }

    @SuppressWarnings("unchecked")
    public void clear() {
        this.elements = (E[]) new Object[DEFAULT_CAPACITY];
        this.size = 0;
    }

    public E get(int index) { // O(1)
        isOutOfBoundsWithLength(index);
        return elements[index];
    }

    private void isOutOfBoundsWithLength(int index) {
        if (index >= elements.length || index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

    public E set(int index, E element) {
        E temp = elements[index];
        System.out.println(temp);
        elements[index] = element;
        return temp;
    }

    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < elements.length; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < elements.length; i++) {
                if (o.equals(elements[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    public int lastIndexOf(Object o) {
        if (o == null) {
            for (int i = elements.length - 1; i >= 0; i--) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = elements.length - 1; i >= 0; i--) {
                if (o.equals(elements[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    public ListIterator listIterator() {
        return null;
    }

    public ListIterator listIterator(int index) {
        return null;
    }

    public List<E> subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || toIndex > size) {
            throw new IndexOutOfBoundsException();
        }
        if (fromIndex > toIndex) {
            throw new IllegalArgumentException();
        }
        List<E> toReturn = new ArrayList<>();
        for (int i = fromIndex; i < toIndex; i++) {
            toReturn.add(elements[i]);
        }
        return toReturn;
    }

    public boolean retainAll(Collection c) { // todo: all
        return false;
    }

    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        if (c.contains(null)) {
            throw new NullPointerException();
        }
        for (Object element : c) {
            if (remove(element)) {
                changed = true;
            }
        }
        return changed;
    }

    public Object[] toArray() {
        return Arrays.copyOf(elements, size);
    }

    public Object[] toArray(Object[] a) { // todo: all
        return new Object[0];
    }
}
