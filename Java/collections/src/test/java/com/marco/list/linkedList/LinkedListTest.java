package com.marco.list.linkedList;

import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class LinkedListTest {
    private LinkedList<Integer> lk;
    @Before
    public void setUp() {
        lk = new LinkedList<>();
    }

    @Test
    public void isEmpty() {
        assertTrue("List should be empty", lk.isEmpty());
        lk.add(1);
        assertFalse("List should not be empty", lk.isEmpty());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void get() {
        lk.addFirst(1);
        assertEquals("Add element 1 and get it", (Integer) 1, lk.get(0));

        lk.get(5); //outOfBoundsException here
    }

    @Test
    public void addFirst() {
        lk.addFirst(1);
        assertEquals("addFirst with and empty list", (Integer)1, lk.get(0));

        lk.addFirst(0);
        assertEquals("First item in the list should be 0", (Integer)0, lk.get(0));
    }

    @Test
    public void addLast() {
        lk.addLast(0);
        assertEquals("AddLast with an empty list", (Integer)0, lk.get(0));

        lk.addLast(1);
        lk.addLast(5);
        assertEquals("Last item in the list should be 5", (Integer)5, lk.get(2));
    }

    @Test
    public void add() {
        assertTrue(lk.add(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addAtIndex() {
        lk.add(0);
        lk.add(1);
        lk.add(3);

        lk.add(2, 2); // Middle index
        assertEquals("Index 2 should be 2", (Integer) 2,lk.get(2));

        lk.add(0, 10); // First index
        assertEquals("Index 0 should be 10", (Integer) 10,lk.get(0));
        lk.add(lk.size(), 20); // Last index
        assertEquals("Last element should be 20", (Integer) 20,lk.get(lk.size() - 1));

        lk.add(3, null);
        assertNull("Index 3 should be null", lk.get(3));

        lk.add(3, 99);
        assertNull("Index 4 should be null", lk.get(4));

        LinkedList<Integer> test = new LinkedList<>();
        test.add(2, 50);
    }

    @Test(expected = NullPointerException.class)
    public void addAllWithANullCollection() {
        java.util.LinkedList<Integer> linkedListJava = null;
        lk.addAll(linkedListJava);
    }

    @Test
    public void addAll() {
        lk.add(1);
        lk.add(2);
        java.util.LinkedList<Integer> linkedListJava = new java.util.LinkedList<>();
        linkedListJava.add(3);
        linkedListJava.add(4);

        lk.addAll(linkedListJava);
        Integer[] arrToCompare = new Integer[]{1,2,3,4};
        assertArrayEquals("List should be 1-2-3-4", arrToCompare, lk.toArray());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addAll_atIndex() {
        lk.add(1);
        lk.add(2);
        lk.add(5);

        java.util.LinkedList<Integer> arrayToAdd = new java.util.LinkedList<>();
        arrayToAdd.add(3);
        arrayToAdd.add(4);

        lk.addAll(2, arrayToAdd);
        Integer[] compareStr = new Integer[]{1, 2, 3, 4, 5};
        assertArrayEquals(compareStr, lk.toArray());

        lk.addAll(15, arrayToAdd); // Exception here
    }

    @Test(expected = NullPointerException.class)
    public void addAll_atIndex_Null() {
        java.util.LinkedList list = null;
        assertTrue(lk.addAll(0, list));
    }

    @Test(expected = NoSuchElementException.class)
    public void getFirst() {
        lk.getFirst();

        lk.add(1);
        assertEquals("First element should be 1", (Integer) 1, lk.getFirst());
    }

    @Test(expected = NoSuchElementException.class)
    public void getLast() {
        lk.getLast();

        lk.add(1);
        lk.add(2);
        lk.add(3);
        assertEquals("Last element should be 3", (Integer) 3, lk.getLast());
    }

    @Test
    public void clear() {
        lk.add(1);
        lk.add(2);
        assertEquals(2, lk.size());
        lk.clear();
        assertEquals("Size should be 0", 0, lk.size());
        assertNull(lk.getNode(0));
    }

    @Test
    public void offerFirstAndOfferLast() {
        lk.add(1);
        lk.offerFirst(0);
        assertEquals("First element should be 0", (Integer) 0, lk.getFirst());
        lk.add(2);
        lk.offerLast(3);
        assertEquals("Last element should be 3", (Integer) 3, lk.getLast());
    }

    @Test
    public void offer() {
        lk.add(1);
        lk.offer(2);
        assertEquals("Second element should be 2", (Integer) 2, lk.get(1));
    }

    @Test
    public void contains() {
        assertFalse("List should not contain anything", lk.contains(0));
        lk.add(0);
        lk.add(1);
        lk.add(2);
        lk.add(null);
        assertTrue("List should contain item 1",lk.contains(1));
        assertTrue("List should contain a null element", lk.contains(null));
    }

    @Test(expected = NoSuchElementException.class)
    public void element() {
        lk.add(1);
        lk.add(2);
        assertEquals("Element should be 1", (Integer) 1, lk.element());

        LinkedList<Integer> testExcp = new LinkedList<>();
        testExcp.element();
    }

    @Test
    public void indexOf() {
        lk.add(5);
        lk.add(10);
        lk.add(15);
        lk.add(null);
        lk.add(20);
        assertEquals("10 should be at index 1", 1, lk.indexOf(10));
        assertEquals("NULL should be at index 3", 3, lk.indexOf(null));
        assertEquals("20 should be at index 4", 4, lk.indexOf(20));
    }

    @Test
    public void lastIndexOf() {
        lk.add(5);
        lk.add(10);
        lk.add(15);
        lk.add(null);
        lk.add(20);
        lk.add(10);
        lk.add(25);
        lk.add(null);

        assertEquals("last index of 5 should be index 0", -1, lk.lastIndexOf(4));
        assertEquals("last index of 10 should be index 5", 5, lk.lastIndexOf(10));
        assertEquals("last index of 15 should be index 2", 2, lk.lastIndexOf(15));
        assertEquals("last index of null should be index 7", 7, lk.lastIndexOf(null));
    }

    @Test(expected = NoSuchElementException.class)
    public void peek() {
        lk.add(1);
        lk.add(2);
        assertEquals("Element should be 1", (Integer) 1, lk.peek());

        LinkedList<Integer> testExcp = new LinkedList<>();
        testExcp.peek();
    }

    @Test(expected = NoSuchElementException.class)
    public void peekFirst() {
        lk.add(1);
        assertEquals("First element should be 1", (Integer) 1, lk.peekFirst());

        LinkedList<Integer> testExcp = new LinkedList<>();
        testExcp.peekFirst();
    }

    @Test(expected = NoSuchElementException.class)
    public void peekLast() {
        lk.add(1);
        assertEquals("First element should be 1", (Integer) 1, lk.peekLast());

        LinkedList<Integer> testExcp = new LinkedList<>();
        testExcp.peekLast();
    }

    @Test
    public void pollFirst() {
        assertNull("pollFirst should return null", lk.pollFirst());
        lk.add(1);
        lk.add(2);
        assertEquals("First element should be 1", (Integer) 1, lk.pollFirst());
    }

    @Test
    public void poll() {
        assertNull("poll should return null", lk.poll());
        lk.add(1);
        lk.add(2);
        assertEquals("First element should be 1", (Integer) 1, lk.poll());
    }

    @Test
    public void pollLast() {
        assertNull("pollLast should return null", lk.pollLast());
        lk.add(1);
        lk.add(2);
        lk.add(3);
        assertEquals("Last element should be 3", (Integer) 3, lk.pollLast());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeIndex() {
        lk.add(0);
        lk.add(1);
        lk.add(2);
        lk.add(3);

        lk.remove(0);
        assertEquals("After removing first item, first now should be 1", (Integer) 1, lk.get(0));
        lk.remove(2);
        assertEquals("After removing third item, second now should be 2", (Integer) 2, lk.get(1));

        LinkedList<Integer> testExcp = new LinkedList<>();
        testExcp.remove(1); //Throws exception
    }

    @Test
    public void removeObject() {
        lk.add(20);
        lk.add(40);
        lk.add(60);
        lk.add(null);
        lk.add(80);

        assertTrue("Removing a null element", lk.remove(null));
        assertTrue("Removing element 80", lk.remove((Integer)80));
        assertFalse("Removing a non-existing element", lk.remove((Integer) 5));
    }

    @Test(expected = NoSuchElementException.class)
    public void removeFirst() {
        lk.add(1);
        lk.add(2);
        lk.add(3);
        assertEquals("Should remove element 1 at index 0", (Integer) 1, lk.removeFirst());

        LinkedList<Integer> testExcp = new LinkedList<>();
        testExcp.removeFirst(); //throws exception
    }

    @Test(expected = NoSuchElementException.class)
    public void remove() {
        lk.add(1);
        lk.add(2);
        lk.add(3);
        assertEquals("Should remove element 1 at index 0", (Integer) 1, lk.remove());

        LinkedList<Integer> testExcp = new LinkedList<>();
        testExcp.remove(); //throws exception
    }

    @Test(expected = NoSuchElementException.class)
    public void removeLast() {
        lk.add(1);
        lk.add(2);
        lk.add(3);
        lk.add(4);
        assertEquals("Last element removed should be 4", (Integer) 4, lk.removeLast());

        LinkedList<Integer> testExcp = new LinkedList<>();
        testExcp.removeLast(); //Throws exception
    }

    @Test
    public void removeFirstOccurrence() {
        lk.add(10);
        lk.add(20);
        assertFalse("Should return false on removing a occurrence that is not found", lk.removeFirstOccurrence(5));
        assertTrue("Should return true on a found occurrence", lk.removeFirstOccurrence(20));
        assertFalse("Should not contain the element 20", lk.contains(20));
    }

    @Test
    public void removeLastOccurrence() {
        lk.add(10);
        lk.add(20);
        lk.add(30);
        lk.add(20);
        lk.add(40);
        lk.add(20);
        assertFalse("Should return false on removing a occurrence that is not found", lk.removeLastOccurrence(5));
        assertTrue("Should return true removing last occurrence of 20", lk.removeLastOccurrence(20));
        assertEquals("Should not contain the last element 20", (Integer) 40, lk.get(4));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void set() {
        lk.add(1);
        lk.add(1);
        lk.add(2);

        assertEquals("Defining index 0 with element 0", (Integer) 1, lk.set(0, 0));
        lk.set(10, 5); // Throws exception
    }

    @Test
    public void Iterator() {
        lk.add(1);
        lk.add(2);
        lk.add(3);

        Integer[] arr = new Integer[]{1, 2, 3};
        int i = 0;
        Iterator iter = lk.iterator();
        while (iter.hasNext()) {
            assertEquals(arr[i], iter.next());
        }
    }

    @Test
    public void testLoop() {
        lk.add(1);
        lk.add(2);
        lk.add(3);
        lk.add(4);

        assertFalse(lk.checkLoop());
    }
}
