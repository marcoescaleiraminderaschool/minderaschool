package com.marco.list.arrayList;

import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

public class ArrayListTest {
    private ArrayList<String> strList;

    @Before
    public void setUp() {
        strList = new ArrayList<>();
    }

    @Test
    public void size(){
        assertEquals("Check if ArrayList has size 0", 0, strList.size());
        strList.add("one");
        assertEquals("Check if ArrayList has size 1", 1, strList.size());
    }

    // DESCRIPTION: Check if ArrayList is empty and not empty
    @Test
    public void empty() {
        assertTrue("Check if ArrayList is empty", strList.isEmpty());
        strList.add("one");
        assertFalse("Check if ArrayList is NOT empty", strList.isEmpty());
    }

    // DESCRIPTION: Add an element
    @Test
    public void add() {
        strList.add("one");
        assertEquals("one", strList.get(0));
    }

    // DESCRIPTION:  Add an element when the capacity of array is full
    //   Default capacity is 10, if we reach it when we try to add an element first
    //     it's suppose to add more 10 null elements to the ArrayList and then insert the wanted element.
    @Test
    public void add_when_capacity_is_full() {
        for (int i = 0; i <= 10; i++) { //When i = 10 it will duplicate the size of the list
            strList.add("inserted");
        }
        assertNotNull(strList.get(10));
        assertNull(strList.get(19));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void add_atIndex_outOfBounds() {
        strList.add("one");
        strList.add("two");
        strList.add("four");
        strList.add("five");
        strList.add("six");

        strList.add(-2, "zero");
    }

    @Test
    public void add_atIndex_start() {
        strList.add("one");
        strList.add("two");
        strList.add("four");
        strList.add("five");
        strList.add("six");

        strList.add(0, "zero");
    }

    @Test
    public void add_atIndex_middle() {
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("four");
        strList.add("five");

        strList.add(3, "three");
    }

    @Test
    public void add_atIndex_last() {
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");

        strList.add(5, "five");
    }

    @Test
    public void add_atIndex_duplicate() {
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.add("five");
        strList.add("six");
        strList.add("seven");
        strList.add("eight");

        strList.add(9, "nine");
    }

    // DESCRIPTION: Get an element with index of the ArrayList
    @Test
    public void get_an_element() {
        strList.add("five");
        strList.add("ten");
        strList.add("fifteen");
        assertEquals("ten", strList.get(1));
    }

    // DESCRIPTION: Get an element that is at an IndexOutOfBounds
    @Test(expected = IndexOutOfBoundsException.class)
    public void get_an_element_with_an_IndexOutOfBoundsException() {
        strList.add("one");   // Index: 0
        strList.add("two");   // Index: 1
        strList.add("three"); // Index: 2

        strList.get(10); // Index: 10 doesn't exist and it's the max default capacity (elements until the length of the list will be null until they are filled)
    }

    // DESCRIPTION: Test toArray method
    @Test
    public void toArray() {
        strList.add("one");
        strList.add("two");
        strList.add("three");
        String[] arrToCompare = new String[]{"one", "two", "three"};
        assertArrayEquals(arrToCompare, strList.toArray());
    }

    //DESCRIPTION: Test IndexOf passing a null object
    @Test
    public void indexOf_Null() {
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.add("five");
        assertEquals(5, strList.indexOf(null)); // index: 5, is the first null element in the list
    }

    // DESCRIPTION: Test IndexOf
    @Test
    public void indexOf() {
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.add("five");
        assertEquals(3, strList.indexOf("four"));
    }

    // DESCRIPTION: Test indexOf when not found
    @Test
    public void indexOf_NotFound() {
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.add("five");
        assertEquals(-1, strList.indexOf("six"));
    }

    // DESCRIPTION: Test lastIndexOf passing a null object
    @Test
    public void lastIndexOf_Null() {
        strList.add("one");
        strList.add("eighty eight");
        strList.add("nine");
        strList.add("seventeen");
        strList.add("seventeen");
        strList.add("nine");
        strList.add("seventeen");
        strList.add("twenty seven");
        strList.add("one");
        assertEquals(9, strList.lastIndexOf(null));
    }

    // DESCRIPTION: Test lastIndexOf
    @Test
    public void lastIndexOf() {
        strList.add("one");
        strList.add("eighty eight");
        strList.add("nine");
        strList.add("seventeen");
        strList.add("seventeen");
        strList.add("nine");
        strList.add("seventeen");
        strList.add("twenty seven");
        strList.add("one");
        assertEquals(8, strList.lastIndexOf("one"));
        assertEquals(6, strList.lastIndexOf("seventeen"));
    }

    // DESCRIPTION: Test lastIndexOf when not found
    @Test
    public void lastIndexOf_NotFound() {
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.add("five");
        assertEquals(-1, strList.indexOf("ten"));
    }

    // DESCRIPTION: Test contains null
    @Test
    public void contains_null() {
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.add("five");
        assertTrue(strList.contains(null)); //Suppose to contain null at index 5
    }

    // DESCRIPTION: Test contains
    @Test
    public void contains() {
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.add("five");
        assertTrue(strList.contains("four")); //Suppose to contain four at index 3
    }

    // DESCRIPTION: Test contains false
    @Test
    public void contains_false() {
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.add("five");
        assertFalse(strList.contains("six")); //Suppose to not contain and return false
    }


    // DESCRIPTION: check if list is cleared with clear()
    @Test
    public void clear() {
        strList.add("filled");
        strList.clear();
        assertArrayEquals(new String[0], strList.toArray());
        assertEquals(0, strList.size());
    }

    // DESCRIPTION: remove outOfBounds
    @Test(expected = IndexOutOfBoundsException.class)
    public void remove_outOfBounds(){
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.remove(11);
    }

    // DESCRIPTION: remove the first index
    @Test
    public void remove_first(){
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.remove(0);

        String[] arr = new String[]{"one", "two", "three", "four"};
        assertArrayEquals(arr, strList.toArray());
    }

    // DESCRIPTION: remove the middle index
    @Test
    public void remove_middle(){
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.remove(2);

        String[] arr = new String[]{"zero", "one", "three", "four"};
        assertArrayEquals(arr, strList.toArray());
    }

    // DESCRIPTION: remove the last index
    @Test
    public void remove_last(){
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.remove(4);

        String[] arr = new String[]{"zero", "one", "two", "three"};
        assertArrayEquals(arr, strList.toArray());
    }

    // DESCRIPTION: remove object false
    @Test
    public void remove_object_false(){
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.remove("eleven");
    }

    // DESCRIPTION: remove object on the first index
    @Test
    public void remove_object_first(){
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.remove("zero");

        String[] arr = new String[]{"one", "two", "three", "four"};
        assertArrayEquals(arr, strList.toArray());
    }

    // DESCRIPTION: remove object on middle index
    @Test
    public void remove_object_middle(){
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.remove("two");

        String[] arr = new String[]{"zero", "one", "three", "four"};
        assertArrayEquals(arr, strList.toArray());
    }

    // DESCRIPTION: remove object on last index
    @Test
    public void remove_object_last(){
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.remove("four");

        String[] arr = new String[]{"zero", "one", "two", "three"};
        assertArrayEquals(arr, strList.toArray());
    }


    // DESCIPTION: set a new value in a already defined element
    @Test
    public void set() {
        strList.add("zero");
        strList.add("two");
        strList.add("two");
        assertEquals("two", strList.set(1, "one"));
    }


    // DESCRIPTION: addAll to the end
    @Test
    public void addAll() {
        strList.add("one");
        strList.add("two");
        java.util.ArrayList<String> arrayListJava = new java.util.ArrayList<String>() {{
           add("three");
           add("four");
        }};
        strList.addAll(arrayListJava);
        String[] compareStr = new String[]{"one", "two", "three", "four"};
        assertArrayEquals(compareStr, strList.toArray());
    }

    // DESCRIPTION: addAll nullPointerException
    @Test(expected = NullPointerException.class)
    public void addAll_NullPointerException() {
        strList.add("one");
        strList.add("two");
        java.util.ArrayList<String> arrayListJava = null;
        strList.addAll(arrayListJava);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addAll_atIndex() {
        strList.add("one");
        strList.add("two");
        strList.add("five");

        java.util.ArrayList<String> arrayToAdd = new java.util.ArrayList<>();
        arrayToAdd.add("three");
        arrayToAdd.add("four");

        strList.addAll(2, arrayToAdd);
        String[] compareStr = new String[]{"one", "two", "three", "four", "five"};
        assertArrayEquals(compareStr, strList.toArray());

        strList.addAll(15, arrayToAdd); // Exception here
    }

    @Test(expected = NullPointerException.class)
    public void addAll_atIndex_Null() {
        java.util.ArrayList list = null;
        assertTrue(strList.addAll(0, list));
    }

    // DESCRIPTION: removeAll Null pointer exception
    @Test(expected = NullPointerException.class)
    public void removeAll_nullPointerException() {
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        java.util.ArrayList<String> arrayListJava = new java.util.ArrayList<String>() {{
            add("three");
            add(null);
            add("four");
        }};
        strList.removeAll(arrayListJava);
    }

    // DESCRIPTION: removeAll
    @Test
    public void removeAll() {
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        java.util.ArrayList<String> arrayListJava = new java.util.ArrayList<String>() {{
            add("three");
            add("four");
        }};
        strList.removeAll(arrayListJava);
        String[] compareStr = new String[]{"one", "two"};
        assertArrayEquals(compareStr, strList.toArray());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void subListOutOfBounds() {
        strList.subList(-1, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subListIllegalArgument() {
        strList.subList(5, 0);
    }

    @Test
    public void subList() {
        strList.add("zero");
        strList.add("one");
        strList.add("two");
        strList.add("three");
        strList.add("four");
        strList.add("five");

        List<String> subListArray = strList.subList(2, 4);
        String[] arrToCompare = new String[]{"two", "three"};
        assertArrayEquals(arrToCompare, subListArray.toArray());
    }

    @Test(expected = NullPointerException.class)
    public void containsAll() {
        strList.add("one");
        strList.add("two");
        strList.add("three");

        java.util.ArrayList<String> listToAdd = new java.util.ArrayList<>();

        listToAdd.add("one");
        listToAdd.add("two");
        assertTrue("Should contain all", strList.containsAll(listToAdd));

        listToAdd.add("four");
        assertFalse("Should not contain all (four isn't on the list)", strList.containsAll(listToAdd));

        listToAdd = null;
        assertTrue("Should throw a exception", strList.containsAll(listToAdd));
    }

    @Test(expected = NullPointerException.class)
    public void containsAllWithANullElementOnTheCollection() {
        strList.add("one");
        strList.add("two");
        strList.add("three");

        java.util.ArrayList<String> listToAdd = new java.util.ArrayList<>();

        listToAdd.add("one");
        listToAdd.add(null);
        listToAdd.add("two");
        assertFalse("Should throw exception because list has a null element",strList.containsAll(listToAdd));
    }

    @Test
    public void Iterator() {
        strList.add("one");
        strList.add("two");
        strList.add("three");

        String[] str = new String[]{"one", "two", "three"};
        int i = 0;
        Iterator itr = strList.iterator();
        while (itr.hasNext()) {
            assertEquals(itr.next(), str[i]);
            i ++;
        }
    }
}