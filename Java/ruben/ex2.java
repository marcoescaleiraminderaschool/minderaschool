/*

Build a Java program that checks whether or not a SODOKU solution is correct. The correction is verified if there are no repeated numbers in the rows, columns, or in each of the 9 3x3 sub-matrices.

Sample Inputs:

[[1 4 5 3 2 7 6 9 8] ☑️
 [8 3 9 6 5 4 1 2 7] ☑️
 [6 7 2 9 1 8 5 4 3] ☑️
 [4 9 6 1 8 5 3 7 2] ☑️
 [2 1 8 4 7 3 9 5 6] ☑️
 [7 5 3 2 9 6 4 8 1] ☑️
 [3 6 7 5 4 2 8 1 9] ☑️
 [9 8 4 7 6 1 2 3 5] ☑️
 [5 2 1 8 3 9 7 6 4]]☑️
 // valid

 [[1 4 5 3 2 7 6 9 8]☑️
 [8 3 9 6 5 4 1 2 7] ☑️
 [6 7 2 9 1 8 5 4 3] ☑️
 [4 9 6 1 8 5 3 7 2] ☑️
 [2 1 8 4 7 3 9 5 6] ☑️
 [7 5 3 2 9 6 4 8 1] ☑️
 [3 6 7 5 4 2 8 1 9] ☑️
 [1 8 4 7 6 1 2 3 5] X
 [5 2 1 8 3 9 7 6 4]] ☑️
  // invalid

 */

public class ex2 {

    static int[][] invalidMatrix={

            {5,3,4,6,7,8,6,1,2},
            {6,7,2,1,9,5,3,4,8},
            {1,9,8,5,4,2,5,6,7},
            {8,5,9,7,6,1,4,2,3},
            {4,2,6,8,5,3,7,9,1},
            {7,1,3,9,1,4,8,5,6},
            {9,6,1,5,3,2,2,8,4},
            {2,8,7,4,1,9,6,3,5},
            {3,4,5,2,8,3,1,7,9}
    };

    static int[][] validMatrix={

            {5,3,4,6,7,8,9,1,2},
            {6,7,2,1,9,5,3,4,8},
            {1,9,8,3,4,2,5,6,7},
            {8,5,9,7,6,1,4,2,3},
            {4,2,6,8,5,3,7,9,1},
            {7,1,3,9,2,4,8,5,6},
            {9,6,1,5,3,7,2,8,4},
            {2,8,7,4,1,9,6,3,5},
            {3,4,5,2,8,6,1,7,9}
    };

    public static void main(String[] args) {
        check(validMatrix);
        // check(invalidMatrix);
    }

    public static void check(int[][] array) {
        boolean checkVal = true;

        for (int y = 0; y < array.length; y++) { // Go through each Y of the array
            int[] nums = new int[9];
            for (int x = 0; x < array[y].length; x++) {
                for(int i = 0; i < nums.length; i++) {
                    if(array[y][x] == nums[i])
                        checkVal = false;
                    else
                        nums[i] = array[y][x];
                }
            }
        }

        String answer = (checkVal) ? "Sudoku validation true!" : "Sudoku validation false!";
        System.out.println(answer);
    }
}
