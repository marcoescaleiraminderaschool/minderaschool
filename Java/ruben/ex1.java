//Exercicio 1 com o Ruben
/*

Write a Java modular program to read a matrix of integers and display that matrix with the rows sorted by descending order of the sums of their elements. The size of the matrix must be defined by the user.

Sample Input:

1 2 5 // sum: 8
3 1 0 // sum: 4
4 9 6 // sum: 19
6 9 5 // sum: 20
Sample Output:

6 9 5 // sum: 20
4 9 6 // sum: 19
1 2 5 // sum: 8
3 1 0 // sum: 4
 */

import java.util.Random;
import java.util.Scanner;

public class ex1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("x: ");
        int x = scanner.nextInt();

        System.out.print("y: ");
        int y = scanner.nextInt();

        System.out.print("Range: ");
        int bound = scanner.nextInt();

        int[][] grid = new int[y][x];

        System.out.println();

        fillGrid(grid, bound);

        sumOrderAndPrint(grid);
    }

    /*
    public static int[][] sortedArrayBySumRow(int[][] grid) {
        Arrays.sort(grid, Comparator.comparingInt(a -> IntStream.of(a).sum()));

        return grid;
    }
    */

    public static void sumOrderAndPrint(int[][] grid) {
        int row = grid.length, column = grid[0].length;

        int[] rowsSum  = new int[row];
        int[][] tempArray  = new int[row][column];

        for (int i = 0; i < grid.length; i++) //Duplicate the orginal a array to a temporary one
            for (int j = 0; j < grid[0].length; j++)
                tempArray[i][j] = grid[i][j];


        for (int y = 0; y < row; y++) //Get the sums of the rows
            for (int x = 0; x < column; x++)
                rowsSum[y] += grid[y][x];

        int temp;
        int i = 0;

        for (int j = column; j > 0; j--) { //Order it
            for (i = 1; i < j; i++) {
                if (rowsSum[i - 1] < rowsSum[i]) {
                    temp = rowsSum[i - 1];
                    rowsSum[i - 1] = rowsSum[i];
                    rowsSum[i] = temp;

                    //Swap rows to the temporary array
                    int[] temporary = tempArray[i - 1];
                    tempArray[i - 1] = tempArray[i];
                    tempArray[i] = temporary;
                }
            }
        }

        printArray(tempArray);
    }

    public static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            int sum = 0;
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " | ");
                sum += array[i][j];
            }
            System.out.println("Sum: " + sum);
        }
    }

    public static void fillGrid(int[][] grid, int bound) {
        Random rand = new Random();

        for(int y = 0; y < grid.length; y++) {
            for(int x = 0; x < grid[0].length; x++)
                grid[y][x] = rand.nextInt(bound);
        }
    }
}
