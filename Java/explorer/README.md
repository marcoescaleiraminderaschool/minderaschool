# Explorer project - Marcelo Silva & Marco Escaleira

---

[![N|Solid](https://avatars3.githubusercontent.com/u/43017609?s=280&v=4)](https://nodesource.com/products/nsolid)


# Phases
---
Initial interaction
---
	
We started by analyzing the initial example and doing some tests, make robot drive forward without colliding on the walls with the help of the three obstacle sensors, this still with noise on the sensors. Most of that time we were messing around with the sensors (beacon, compass, ground, bumper, obstacle sensors).


Remove noise
---

After investigating we removed the noise from the simulator. It made our lifes easier on the development process has we didn't need to worry about the noise disturbing the sensors. 


Use Trello as guide  
--- 
It was defined and carried out the purpose of making the mouse drive forward without colliding and performing a controlled rotation and that was done with extreme success!!!
	
	
Return home
---
Unfortunately we couldn't finish the return of the robot to his initial position. Our idea of making a Stack to record all steps that robot does to get into beacon area but with inverted turns, so it can do the same path back.


# Installing
---

__First off__ you need to clone this repository and do a docker build in the main simulator.


Process:


```sh
$ git clone http://bitbucket.org/mmmindera/explorer.git

$ cd explorer-tools/
$ docker build -t mindera-school/explorer-tools:latest .
```

# Deployment
---
```sh
$ cd ../explorer-agent
$ mvn clean package
$ docker build -t mindera-school/explorer-agent:latest .

$ ./start
```

# Built With
---
* [Java](https://docs.oracle.com/javase/8/docs/) - The development language
* [Maven](https://maven.apache.org/) - Dependency Management


# Acknowledgments
---
* Patience
* Patience
* And more pacience