package com.minderaschool.explorer.agent;

import com.minderaschool.explorer.client.*;

import java.util.Arrays;
import java.util.Stack;

public class Brain {
    private Stack<StateStack> movements = new Stack<>();
    private int phase = 0;
    private int returningHomeRotate = 0;

    // States of the robot
    enum State {
        ROTATE, DRIVE, WAIT, DECIDE, WANDER, STOP, RETURN
    }

    private State currentState;

    //Possible directions of the robot
    enum Direction {
        RIGHT, BOTTOM, LEFT, TOP
    }

    private Direction currentDirection;

    //Main vars needed for robot behavior (name, sensors, ...)
    private Robot robot;
    private String robName;
    private double center, left, right, compass;
    private boolean bumper;
    private int ground;
    private BeaconMeasure beacon;
    private char turning;

    //Vars for defining rotation
    private double startTurnTime, currentTime, waitTime, wanderTime;
    private final double TIME_TO_ROTATE = 6.0;

    // Square initialization
    //private Square currentSquare = new Square();
    //int sideCounter = 0;

    //Default constructor
    Brain(String robName, int pos, String host) {
        robot = new Robot();
        ground = -1;
        currentDirection = getCurrentDirection();
        System.out.println(currentDirection);
        currentState = State.DECIDE;
        this.robName = robName;
        robot.initRobot(robName, pos, host); // register robot in simulator
        robot.setVisitingLed(true);

    }

    //Routine to check time before starting
    private void timeOnStart() {
        robot.readSensors();
        double time = robot.getTime();
        System.out.println(time + ": Waiting to start");
        while (time <= 0.0001 && time >= -0.0001) {
            // do nothing, just wait until it starts
            robot.readSensors();
            time = robot.getTime();
        }
        System.out.println("\n" + time + ": Started");
    }

    //This is where the code starts being executed when play button is clicked
    public void mainLoop() {
        timeOnStart();

        while (true) {
            robot.readSensors();
            loop();
        }
    }

    //Get and define the needed sensors
    private void getSensors() {
        if (robot.isObstacleReady(0))
            center = robot.getObstacleSensor(0);
        if (robot.isObstacleReady(1))
            left = robot.getObstacleSensor(1);
        if (robot.isObstacleReady(2))
            right = robot.getObstacleSensor(2);
        if (robot.isCompassReady())
            compass = robot.getCompassSensor();
        if (robot.isBumperReady())
            bumper = robot.getBumperSensor();
        if (robot.isGroundReady())
            ground = robot.getGroundSensor();
        if (robot.isBeaconReady(0)) {
            beacon = robot.getBeaconSensor(0);
        }
    }

    //The method that defines what the robot will do
    private void decide() {
        //System.out.println("DECIDE " + currentTime + " COMPASS(" + compass + ")" + " BEACON(" + beacon.beaconVisible + "," + beacon.beaconDir + ") " + currentState);
        if (ground == 0) { //If ground is 0 the robot is at beacon site
            robot.setVisitingLed(false);
            robot.setReturningLed(true);
            phase = 1;
            currentState = State.RETURN;
        }

        if (beacon.beaconVisible && phase == 0) {
            if (beacon.beaconDir > 10) {
                turning = 'l';
                currentState = State.ROTATE;
            } else if (beacon.beaconDir < -10) {
                turning = 'r';
                currentState = State.ROTATE;
            } else {
                currentState = State.DRIVE;
            }
        } else {
            currentState = State.WANDER;
        }
        if (phase == 1) {
            currentState = State.RETURN;
        }
    }

    private void loop() {
        getSensors();

        currentTime = robot.getTime();
        currentDirection = getCurrentDirection();
        //System.out.println(currentState + " " + currentTime + " COMPASS(" + compass + ")" + " BEACON(" + beacon.beaconVisible + "," + beacon.beaconDir + ") GROUND(" + ground + ")");

        switch (currentState) {
            case DECIDE:
                decide();
                break;
            case DRIVE:
                drive();
                break;
            case ROTATE:
                rotate();
                break;
            case WAIT:
                waitTime();
                break;
            case WANDER:
                wander();
                break;
            case RETURN:
                returnHome();
                break;
        }
    }

    private Direction getCurrentDirection() {
        //System.out.println(compass);
        if (compass < 135 && compass > 45) { //TOP
            return Direction.TOP;
        } else if (compass > -135 && compass < -45) { //BOTTOM
            return Direction.BOTTOM;
        } else if ((compass < -135 && compass > -180) || (compass < 180 && compass > 135)) { //LEFT
            return Direction.LEFT;
        } else if (compass < 45 && compass > -45) { // RIGHT
            return Direction.RIGHT;
        }
        return currentDirection;
    }


    /*
     *
     * AUX
     *
     */
    private void wander() { //Robot goes forward without colliding with walls
        if (wanderTime == 0) {
            wanderTime = currentTime;
            // System.out.println("ROTATING START " + compass + " " + beacon.beaconDir);
        }
        if (currentTime - wanderTime < 20) {
            if (center > 1.5) {
                if (right > left) {
                    turning = 'l';
                    currentState = State.ROTATE;
                } else {
                    turning = 'r';
                    currentState = State.ROTATE;
                }
            } else {
                drive();
                currentState = State.WANDER;
            }
        } else {
            stop();
            wanderTime = 0.0;
            currentState = State.DECIDE;
        }
    }

    private void returnHome() {
        if (returningHomeRotate < 2) {
            robot.driveMotors(0.0, 0.0);
            turning = 'r';
            currentState = State.ROTATE;
            returningHomeRotate++;
        } else {
            State state= State.WAIT;
            StateStack stack = new StateStack();
            if (!movements.isEmpty()) {
                stack = movements.pop();
                state = stack.getCurrentState();
            } else {
                robot.driveMotors(0.0, 0.0);
            }
            switch (state) {
                case DRIVE:
                    drive();
                    break;
                case ROTATE:
                    turning = stack.getTurning();
                    rotate();
                    break;
                case WAIT:
                    waitTime();
                    break;
            }
        }
    }

    private void waitTime() {
        stop();
        if (waitTime == 0) {
            waitTime = currentTime;
        }

        if (phase == 0) movements.push(new StateStack(State.WAIT));

        if (currentTime - waitTime > TIME_TO_ROTATE) {
            waitTime = 0.0;
            currentState = State.DECIDE;
        }
    }


    /*
     *
     * MOVEMENTS
     *
     */
    private void drive() {
        robot.driveMotors(0.1, 0.1);
        if (phase == 0) movements.push(new StateStack(State.DRIVE));
        currentState = State.DECIDE;
    }


    private void rotate() {
        if (startTurnTime == 0) {
            startTurnTime = currentTime;
        }

        if (currentTime - startTurnTime < TIME_TO_ROTATE) {
            if (turning == 'r') {
                robot.driveMotors(0.131, -0.131);
                if (phase == 0) movements.push(new StateStack(State.ROTATE, turning));
                //System.out.println("Rotating COMPASS: " + compass + " BEACON: " + beacon.beaconDir);
            } else if (turning == 'l') {
                robot.driveMotors(-0.131, 0.131);
                if (phase == 0) movements.push(new StateStack(State.ROTATE, turning));
                //System.out.println("Rotating COMPASS: " + compass + " BEACON: " + beacon.beaconDir);
            } else {
                System.out.println("There is no direction to turn");
            }

        } else {
            //System.out.println("Rotating FINISH: " + compass + " BEACON: " + beacon.beaconDir);
            robot.driveMotors(0.0, 0.0);
            startTurnTime = 0.0;
            currentState = State.WAIT;
        }
    }

    private void stop() {
        robot.driveMotors(0.0, 0.0);
        if (phase == 0) movements.push(new StateStack(State.STOP));
        currentState = State.DECIDE;
    }
}