import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageObjects.Homepage;

public class TestExample {

    private Homepage homepage;

    @Test
    public void TestOne(){
        WebDriver driver = new ChromeDriver();

        homepage = new Homepage(driver);

        //goes to the indicated URL
        driver.navigate().to("https://www.kuantokusta.pt");

        //asserts that the google logo is displayed
        Assert.assertTrue(homepage.kuantokustaLogo.isDisplayed());

        //writes something on the search box
        homepage.searchBox.sendKeys("Apple");

        //clicks on the search button
        homepage.searchButton.click();

        //closes the driver's current window
        driver.close();

        //quits the driver
        driver.quit();
    }
}
