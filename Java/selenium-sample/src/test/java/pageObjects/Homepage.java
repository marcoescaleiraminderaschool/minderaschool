package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Homepage {

    public Homepage (WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "img[alt='KuantoKusta logo']")
    public WebElement kuantokustaLogo;

    @FindBy(css = "#search_form > div > input")
    public WebElement searchBox;

    @FindBy(css = "#search_form > div > button")
    public WebElement searchButton;
}
