import java.util.*; 

public class smallestNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int smallestNumber = Integer.MAX_VALUE;
        int number = 0;

        System.out.print("\033\143 How many numbers? \nAnswer: ");
        int x = scanner.nextInt();

        for (int i = 1; i <= x; i++) {
            System.out.printf("\n%d number: ", i);
            number = scanner.nextInt();

            if (number < smallestNumber) {
                smallestNumber = number;
            }
        }
        scanner.close();

        System.out.println("\nThe smallest number is: " + smallestNumber);
    }
}