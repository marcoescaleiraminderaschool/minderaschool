import java.util.*;

public class ParImpar {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = 0;

        System.out.print("Write a number: ");
        number = scanner.nextInt();
        
        scanner.close();

        System.out.println("Even: " + ((number % 2) == 0));
    }
}