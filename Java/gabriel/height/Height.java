import java.util.*;

public class Height {
    public static final int minHeight = 140;
    public static final int superTall = 300;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("What's you height? ");
        int userHeight = scanner.nextInt();

        if (userHeight > minHeight) {
            System.out.println("You can ride the roller coaster");
        } else {
            System.out.println("You are too small to ride the roller coaster");
        }
        if (userHeight > superTall) {
            System.out.println("You are super tall and deserve a witty message");
        }

        scanner.close();
    }
}