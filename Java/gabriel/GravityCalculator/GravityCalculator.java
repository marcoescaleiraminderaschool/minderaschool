import java.util.*;

public class GravityCalculator {
    public static final float GRAVITY = -9.81f;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
       //posição inicial
       System.out.print("Insert the time of fall in seconds: ");
       int time = scanner.nextInt();

       System.out.print("\nInitial velocity (m/s): ");
       float initialVelocity = scanner.nextFloat();

        System.out.print("\nInitial position(m): ");
        float initialPosition = scanner.nextFloat();

        float calculation = (0.5f * GRAVITY * (time*time)) + (initialVelocity * time) + initialPosition;

        System.out.format("\n\nx(%d segundos) = 0.5 x %.2f x %d^2 + %.2f x %d + %.2f", time, GRAVITY, time, initialVelocity, time, initialPosition);
        System.out.printf("\n\nResult: %.2f\n", calculation);
    }
}