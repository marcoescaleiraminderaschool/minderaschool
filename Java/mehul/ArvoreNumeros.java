import java.util.*;

public class ArvoreNumeros {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Altura da arvore: ");
        int treeHeight = scanner.nextInt();

        System.out.print("\033\143");
        
        int r, num;
        
        for (int i = 0; i <= treeHeight; i++) {
            num = 1;
            r = i + 1;
            for (int a = 0; a < treeHeight - i; a++) {
                System.out.print(" ");
            }

            for (int col = 0; col <= i; col++) {
                if (col > 0) {
                    num = num * (r - col) / col;    
                }
                System.out.print(num + " ");
            }
            System.out.println();
        }
        
        scanner.close();
    }
}