import java.util.Arrays;

public class Bank {

    private static int id;
    private Account[] accounts = new Account[1];

    //add
    public int add(Account acc) {
        try {
            accounts     = Arrays.copyOf(accounts,id + 1);
            accounts[id] = acc;

            this.id++;

            return id - 1;
        } catch (Exception e) {
            System.out.print(e);
        }
        return -1;
    }


    //count
    public static int count() { return id; }


    //Get Accounts data
    public void getAllData() {
        System.out.print("|");

        for (int i = 0; i < id; i++)
            System.out.printf("\t[%d] conta: %s | %.2f€", i, accounts[i].getName(), accounts[i].getBalance());

        System.out.print("\t|\n");
    }


    //get
    public Account get(int id) {
        return accounts[id];
        //return "Account name: " + accounts[id].getName() + " has a balance of " + accounts[id].getBalance() + "€";
    }
}
