public class Main {

    public static Bank bank = new Bank();

    public static void main(String[] args) {
        Account marco   = new Account("Marco", 250);
        Account marcelo = new Account("Marcelo", 150);

        bank.add(marco);
        bank.add(marcelo);

        bank.getAllData();
;    }
}