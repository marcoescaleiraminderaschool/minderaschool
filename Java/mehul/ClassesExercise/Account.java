
public class Account {

    private String name;
    private double balance;

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
        //System.out.printf("(%s account created with a balance of %.2f)\n", this.name, this.balance);
    }


    public double getBalance() { return balance; }
    public String getName() { return name; }


    //Withdraw
    public boolean withdrawal(double amount) {
        if (this.balance - amount < 0) {
            System.out.print("(Withdrawal not processed)\n");
            return false;
        } else {
            this.balance -= amount;
            System.out.printf("(Withdrawal processed. Your balance is %.2f)\n", this.balance);
            return true;
        }

    }

    //Transfer
    public boolean transfer(double amount, Account dest) {
        if (withdrawal(amount)) {
            dest.deposit(amount);

            System.out.printf("(%.2f€ transfer processed to %s account)\n", amount, dest.getName());
            return true;
        }
        return false;
    }

    //Deposit
    public void deposit(double amount) {
        this.balance += amount;
        System.out.println("(Deposit processed)");
    }
}
