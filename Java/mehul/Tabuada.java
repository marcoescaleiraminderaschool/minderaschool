import java.util.*;

public class Tabuada {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduce a number: ");
        int number = scanner.nextInt();
        int result = 0;

        
        for (int i = 1; i <= 10; i++) {
            // result = number * i;
            // System.out.println(number + " x " + i + " = " + result);
            System.out.format("%d x %d = %d\n", number, i, (number * i));
        }
        
        
        /*
        int i = 1;
        while(i <= 10) {
            result = number * i;
            System.out.println(number + " x " + i + " = " + result);
            i++;
        }
        */
        scanner.close();
    }
}