import java.util.*;

public class NotasAlunos {
    public static boolean NONE_PASS  = true;
    public static boolean NONE_FAIL  = true;
    public static short   SIZE_ARRAY = 10;
    public static short   MIN_NOTE   = 9;


    public static void main(String[] Args) {
        Scanner scanner = new Scanner(System.in);

        String[]  nomes = new String[SIZE_ARRAY];
        int[]     notas = new int[SIZE_ARRAY];
        boolean[] check = new boolean[SIZE_ARRAY];

        for (int a = 7; a < nomes.length; a++) {
            System.out.print("Nome: ");
            nomes[a] = scanner.next();

            System.out.print("Nota: ");
            notas[a] = scanner.nextInt();

            check[a] = (notas[a] > MIN_NOTE) ? true : false;
        }

        scanner.close();

        System.out.print("\033\143");

        pass(nomes, notas, check);

        fail(nomes, notas, check);
    }

    public static void pass (String[] nomes, int[] notas, boolean check[]) {
        System.out.println("Pass:");
        for (int i = 0; i < check.length; i++) {
            if (check[i]) {
                printResult(i, nomes, notas);
                NONE_PASS = false;
            }
        }
        if (NONE_PASS)
            System.out.print("No student passed");
    }

    public static void fail (String[] nomes, int[] notas, boolean check[]) {
        System.out.println("\n\nFail:");
        for (int y = 0; y < check.length; y++) {
            if (!check[y]) {
                printResult(y, nomes, notas);
                NONE_FAIL = false;
            }
        }
        if (NONE_FAIL)
            System.out.print("No student failed");
    }

    public static void printResult (int i, String[] nomes, int[] notas) {
        System.out.print("\n  "+nomes[i]+ " " + notas[i]);
    }
}