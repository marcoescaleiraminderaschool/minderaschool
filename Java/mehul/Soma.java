import java.util.*;

public class Soma {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = 0;
        int soma = 0;

        do {
            System.out.print("Introduce a number: ");
            number = scanner.nextInt();

            if (number % 2 == 0) {
                soma += number;
                System.out.format("(+%d) %d\n", number, soma);
            } else {
                System.out.println(soma);
            }
        } while(number != 0);

        scanner.close();
    }
}