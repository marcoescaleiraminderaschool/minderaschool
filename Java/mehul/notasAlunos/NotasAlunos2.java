import java.util.*;

public class NotasAlunos2 {
    public static boolean NONE_PASS  = true;
    public static boolean NONE_FAIL  = true;
    public static short   STUDENTS   = 10;
    public static short   MIN_NOTE   = 9;

    public static void printResult (int i, String nome, int nota) {
        System.out.print("\n  " + nome + " " + nota);
    }

    public static void main(String[] Args) {
        Scanner scanner = new Scanner(System.in);

        Student[] student = new Student[STUDENTS];

        String name;
        int    note;

        for (int a = 0; a < STUDENTS; a++) {
            System.out.print("Nome: ");
            name = scanner.next();

            System.out.print("Nota: ");
            note = scanner.nextInt();

            student[a] = new Student(name, note);
        }

        scanner.close();

        System.out.print("\033\143");

        System.out.println("Pass:");
        for(int i = 0; i < student.length; i++) {
            if (student[i].check()) {
                printResult(i, student[i].getName(), student[i].getNote());
                NONE_PASS = false;
            }
        }
        if (NONE_PASS)
            System.out.print("\nNo student passed");

        
        System.out.println("\n\nFail:");
        for(int x = 0; x < student.length; x++) {
            if (!student[x].check()) {
                printResult(x, student[x].getName(), student[x].getNote());
                NONE_FAIL = false;
            }
        }
        if (NONE_FAIL)
            System.out.print("\nNo student failed");
    }
}