public class Student {
    private String name;
    private int note;

    public Student(String name, int note) {
        this.name = name;
        this.note = note;
    }

    String getName() { return this.name; }

    int    getNote() { return this.note; }
    
    boolean check() {
        return (this.note > 9) ? true : false;
    }
}