import java.util.*;

public class Arvore {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("What's the height of the tree?");
        int treeHeight = scanner.nextInt();

        for (int i = 0; i < treeHeight; i++) {
            for (int a = 0; a < treeHeight - i; a++) {
                System.out.print(" ");
            }
            for (int b = 0; b < (2 * i + 1); b++) {
                System.out.print("*");
            }
            System.out.println();
        }

        scanner.close();
    }
}