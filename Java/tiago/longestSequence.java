import java.util.*;

public class longestSequence {

    public static List<Integer> longestConsecutive(int[] numbersToBeProcessed) {

        if (numbersToBeProcessed.length == 0) {
            return null;
        }

        int bestStart  = 0;
        int curStart   = 0;
        int bestLength = 1;
        int curLength  = 1;

        for (int i = 1; i < numbersToBeProcessed.length; i++) {
            if (numbersToBeProcessed[i] > numbersToBeProcessed[i-1]) {
                curLength++;
                if (curLength > bestLength) {
                    bestStart  = curStart;
                    bestLength = curLength;
                }
            } else {
                curStart  = i;
                curLength = 1;
            }
        }
        ArrayList<Integer> ret = new ArrayList<>(bestLength);
        for (int i = 0; i < bestLength; i++) {
            ret.add(numbersToBeProcessed[bestStart+i]);
        }
        return ret;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberInput = 0;
        ArrayList<Integer> numbers = new ArrayList<Integer>();

        do {
            System.out.print("\nInput: ");
            numberInput = scanner.nextInt();

            if(numberInput != 0)
                numbers.add(numberInput);
        } while (numberInput != 0);

        int[] input = new int[numbers.size()];
        for(int i = 0; i < numbers.size(); i++)
            input[i] = numbers.get(i);

        System.out.println("\n Numeros introduzidos: " + Arrays.toString(input));
        System.out.println(longestConsecutive(input));
    }







    /*
    Write a Java program to find the length of the longest consecutive elements sequence from a given unsorted array of integers. When 0 is provided it finishes the array.

    Sample Input:
    49, 1, 3, 200, 2, 4, 70, 5, 0
    Sample Output:
    1, 2, 3, 4, 5
    5
     */
}
