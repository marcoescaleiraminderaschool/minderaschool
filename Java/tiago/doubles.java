import java.util.*;

public class doubles {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = 0;
        ArrayList<Integer> input = new ArrayList<Integer>();

        do {
            System.out.print("\nIntroduce a number: ");
            number = scanner.nextInt();

            if(number != 0)
                input.add(number);
        } while (number != 0);

        int[] output = new int[input.size()];

        int a = 0;

        boolean isRepeated = false;
        for(int i = 0; i < input.size(); i++) {
            isRepeated = check(output, input.get(i));
            if(!isRepeated){
                output[a] = input.get(i);
                a++;
            }
        }

        for (int i = 0;i < a; i++) {
            System.out.print(output[i] + " ");
        }
    }

    public static boolean check(int[] numbers, int numero) {
        for(int x = 0; x < numbers.length; x++){
            if(numero == numbers[x]){
                return true;
            }
        }
        return false;
    }

    /*
    Write a Java program to remove the duplicate elements of a given array and return the new length of the array. When 0 is provided it finishes the array.

        Sample Input:
        20, 20, 30, 40, 50, 50, 50, 0
        Sample Output:
        20, 30, 40, 50
        4
    */
}
