package com.mmm.vaadintest;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Nav;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.NavigationEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

@SpringBootApplication
public class VaadintestApplication {

    public static void main(String[] args) {
        SpringApplication.run(VaadintestApplication.class, args);
    }

    @Route("")
    public static class MainView extends VerticalLayout {
        public MainView(MyService service) {
            add(new Nav(){{
                add(new Button("Home"));
                add(new Button("Projects", e-> {
                    new RouterLink("projects", Projects.class);
                }));
            }});

            add(new Button("Click me", e-> {
                Notification.show(service.sayHi());
            }));
        }
    }

    @Route("projects/")
    public static class Projects extends VerticalLayout {
        public Projects(MyService service) {
            add(new Label("Projects page"));
        }
    }

    @Service
    public static class MyService {
        public String sayHi() {
            return "Hello there2";
        }
    }
}
