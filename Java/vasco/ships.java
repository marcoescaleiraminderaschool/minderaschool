/*

Given a matrix of 10 by 10, ask the user for 10 ships.
1 x ship with 4 squares
2 x ship with 3 squares
3 x ship with 2 squares
4 x ship with 1 square
For each ship, the user should input one or two coordinates, the beggining of the ship, and the end of the ship.
At the end print the board, and then, continuously ask the user for coordinates, as if bombs. The program should terminate hence all ships are sunken.

 */

import java.util.Scanner;

public class ships {
    static boolean win = false;
    static int Y = 10;
    static int X = 10;


    public static void main(String[] args) {
        char[][] matrix = new char[Y][X];
        initGrid(matrix);
        showGrid(matrix);

        for (int i = 1; i < 11; i++) {
            if (i == 1) {
                setShip(4, matrix, i);
                showGrid(matrix);
            } else if (i > 1 && i < 4) {
                setShip(3, matrix, i);
                showGrid(matrix);
            } else if (i > 3 && i < 7) {
                setShip(2, matrix, i);
                showGrid(matrix);
            } else if (i > 6 && i < 11) {
                setShip(1, matrix, i);
                showGrid(matrix);
            }
        }


        while(shipSquaresLeft(matrix) > 0) {
            askBomb(matrix);
            showGrid(matrix);
        }
    }


    public static int shipSquaresLeft(char[][] board) {
        int countSquareShips = 0;
        for(int i = 0; i < board.length; i++) {
            for(int j = 0; j < board[i].length; j++) {
                if(board[i][j] == 'V') {
                    countSquareShips++;
                }
            }
        }

        return countSquareShips;
    }

    public static void askBomb(char[][] board) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Line: ");
        int line = scanner.nextInt();

        System.out.print("Column: ");
        int column = scanner.nextInt();

        if(board[line][column] == '_') {
            System.out.println("WATER!!!!!");
            board[line][column] = 'O';
        } else {
            System.out.println("HIT!!!!!");
            board[line][column] = '\u2718';
        }

    }


    static void setShip(int shipSize, char[][] board, int ship) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nInsert the ship " + ship + " with size of " + shipSize + "\n");

        System.out.println("Start Position ");
        System.out.print("Line: ");
        int startLine = scanner.nextInt();
        System.out.print("Column: ");
        int startColumn = scanner.nextInt();

        int endLine = startLine;
        int endColumn = endLine;

        int opt = 2;
        if (shipSize != 1) {
            System.out.println("\n1 - Vertical");
            System.out.println("2 - Horizontal");

            System.out.print("A: ");
            opt = scanner.nextInt();
        }
        System.out.println("-----------------------");

        switch (opt) {
            case 1:
                endLine = startLine + shipSize - 1;
                endColumn = startColumn;
                break;
            case 2:
            default:
                endLine = startLine;
                endColumn = startColumn + shipSize - 1;
                break;
        }

        for (int y = startLine; y <= endLine; y++)
            for (int x = startColumn; x <= endColumn; x++)
                board[y][x] = '\u2718';

    }


    static void showGrid(char[][] matrix) {
        System.out.printf("   0 1 2 3 4 5 6 7 8 9\n");
        for (int y = 0; y < matrix.length; y++) {
            System.out.printf("%d  ", y);
            for (int x = 0; x < matrix[y].length; x++)
                System.out.print(matrix[y][x] + " ");

            System.out.println();
        }
    }

    static void initGrid(char[][] matrix) {
        for (int y = 0; y < matrix.length; y++)
            for (int x = 0; x < matrix[y].length; x++)
                matrix[y][x] = '_';
    }
}
