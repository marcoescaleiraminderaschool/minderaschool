import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class NumberFightHuman {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int secretNumber = ThreadLocalRandom.current().nextInt(1, 101);
        int answerNumber = 0;
        int trys = 0;
        
        do {
            System.out.println("Your guess: ");
            answerNumber = scanner.nextInt();
            
            if (answerNumber < secretNumber) {
                System.out.println("Go up!\n");
                trys++;
            } else if (answerNumber > secretNumber) {
                System.out.println("Go down!\n");
                trys++;
            }

        } while (secretNumber != answerNumber);
        
        System.out.format("\nVictory || You tried %d times\n", trys);
        
        scanner.close();
    }
}