import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ClassFile {
    public static File file = new File("example.txt");
    public static FileWriter fileWrite;
    public static FileReader fr;
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        printMenu();
    }

    public static void readFromFile(File file) {
        System.out.printf("\nReading file: %s \n", file.getName());
        try {
            fr = new FileReader(file.getAbsolutePath());

            int i;
            while((i = fr.read()) != -1) {
                System.out.print((char) i);
            }
        } catch(IOException exceptionIO) {
            System.out.println(exceptionIO);
        }

        printMenu();
    }

    public static void writeOnFile(File file) {
        ArrayList<String> countries = new ArrayList<>();
        String userInput = "0";
        System.out.println();
        do {
            System.out.print("Insert a country name: ");
            userInput = scanner.next();
            if(!userInput.equals("0"))
                countries.add(userInput);
        } while (!userInput.equals("0"));


        if(!fileExists(file, false)) {
            System.out.println("\nCreate a file first before writing on it.");
        } else {
            try {
                fileWrite = new FileWriter(file);

                for (int i = 0; i < countries.size(); i++) {
                    fileWrite.write(countries.get(i));
                    fileWrite.write(System.lineSeparator());
                }
                fileWrite.close();

                System.out.println("Countries insert with success");
            } catch(IOException exceptionIO) { System.out.println(exceptionIO); }
        }

        printMenu();
    }

    public static void create() {
        try {
            file.createNewFile();
            System.out.println("\nFile has been created! example.txt");
        } catch (IOException exceptionIO) { System.out.println(exceptionIO); }

        printMenu();
    }

    public static void deleteFile(File file) {

        System.out.println("Are u sure you want to delete? (y/n)");
        char opt = scanner.next().charAt(0);

        switch (opt) {
            case 'y':
                file.delete();
                break;
            case 'n':
            default:
                printMenu();
                break;

        }
    }

    public static String absolutePath(File file) { return file.getAbsolutePath(); }

    public static boolean fileExists(File file, boolean message) {
        if (file.exists()) {
            if (message)
                System.out.println("\nThe file already exists");
            return true;
        }
        System.out.println("\nThe file doesn't exist");
        return false;
    }


    public static void printMenu() {
        int opt = 0;
        do {
            System.out.println("\n\n-------------------------------");
            System.out.println("\t\tFile Exercise");
            System.out.println("1 - Create a file");
            System.out.println("2 - Check the file");
            System.out.println("3 - Write on the file");
            System.out.println("4 - Read the file");
            System.out.println("5 - Delete the file");
            System.out.println("0 - Exit");

            System.out.print("\nA: ");

            opt = scanner.nextInt();

            switch (opt) {
                case 1:
                    create();
                    break;
                case 2:
                    fileExists(file, true);
                    break;
                case 3:
                    writeOnFile(file);
                    break;
                case 4:
                    readFromFile(file);
                    break;
                case 5:
                    deleteFile(file);
                    break;
            }
        } while (opt != 0);
    }
}


// Após isso, fazer o jogo do 4 em linha