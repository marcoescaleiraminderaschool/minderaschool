public class args {
    public static void main(String[] args) {
        int sum = 0;
        int[] number = new int[args.length]; //Criar um array primitivo com o tamanho do args
        for (int i=0; i<args.length; i++) { //percorrer o args[]
             number[i] = Integer.parseInt(args[i]); //Converter de String para int
             sum += number[i]; //Somar já com int
             System.out.printf("(+%d) %d\n", number[i], sum); //Adicional
        }
        System.out.println("Final Result: " + sum); //Resultado final
    }
}