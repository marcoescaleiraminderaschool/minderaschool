/*

Write a Java program that asks the user for a number n.
After the user chooses an n, the program should ask for n numbers, creating an array through this process.
After the first n numbers, the program should ask again for n numbers, creating a second array through this process.
With the two arrays given by the user, the program should print a new array with size n, with each element being the sum of the elements in the previous arrays.

example:
input:
3
1
2
3
4
5
6
output:
5 7 9

explanation:
3 // number of elements in the arrays
[1, 2, 3]  // array 1
[4, 5, 6]  // array 2
[5, 7, 9]  // output array
 */

import java.util.Arrays;
import java.util.Scanner;

public class arraySum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 0;

        do { //Get the n number
            System.out.print("Number: ");
            n = scanner.nextInt();
        } while (n == 0);

        //Initialize the arrays
        int[] firstArray  = new int[n];
        int[] secondArray = new int[n];

        System.out.println("First array");
        firstArray  = fillArray(firstArray);
        System.out.println("\nSecond array");
        secondArray = fillArray(secondArray);

        sumArrays(n, firstArray, secondArray);
    }

    static void sumArrays(int n, int[] firstArray, int[] secondArray) {
        int[] sumArray = new int[n]; //it could be firstArray.length too ou the second one as they have the same length

        System.out.println("\n" + Arrays.toString(firstArray) + "\n" + Arrays.toString(secondArray)); //Print the arrays created by the user

        for (int i = 0; i < n; i++)
            sumArray[i] = firstArray[i] + secondArray[i];

        System.out.println("\nSum of both arrays \n" + Arrays.toString(sumArray));
    }

    static int[] fillArray(int[] array) {
        Scanner scanner = new Scanner(System.in);
        int n = 0;

        while(n < array.length) {
            System.out.printf("(%d) number: ", (n + 1));
            array[n] = scanner.nextInt();
            n++;
        }
        return array;
    }
}
