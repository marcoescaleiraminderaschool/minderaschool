/*

Create a program that works as a calculator.
The program should ask the user for the operation to be performed (addition, subtraction, multiplication or division) and should also give the user the option to quit.
After choosing the operation, the program should ask for the operands of that operation, and print the result along with the operands and the operator.
The program should only exit when the user chooses to quit.

 */

import java.util.Scanner;

public class calculator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int opt = 0;

        do {
            printMenu();
            opt = scanner.nextInt();

            switch (opt) {
                case 1:
                    calculate("Addition", '+');
                    break;

                case 2:
                    calculate("Subtraction", '-');
                    break;

                case 3:
                    calculate("Multiplication", '*');
                    break;

                case 4:
                    calculate("Division", '/');
                    break;
            }
        } while (opt != 0);

        System.exit(0);
    }

    public static void calculate(String type, char sign) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("\n\n\t"+ type +" \nFirst operand: ");
        float firstOperand = scanner.nextFloat();

        System.out.print("\nSecond operand: ");
        float secondOperand = scanner.nextFloat();

        switch (sign) {
            case '+':
                System.out.printf("\n%.2f " + sign + " %.2f = %.2f", firstOperand, secondOperand, (firstOperand + secondOperand));
                break;
            case '-':
                System.out.printf("\n%.2f " + sign + " %.2f = %.2f", firstOperand, secondOperand, (firstOperand - secondOperand));
                break;
            case '*':
                System.out.printf("\n%.2f " + sign + " %.2f = %.2f", firstOperand, secondOperand, (firstOperand * secondOperand));
                break;
            case '/':
                System.out.printf("\n%.2f " + sign + " %.2f = %.2f", firstOperand, secondOperand, (firstOperand / secondOperand));
                break;

        }
    }

    static void printMenu() { //Main menu
        System.out.println("\n--------------------------\n\t\tCalculator\n--------------------------");

        System.out.print("\n1 - Addition \n2 - Subtraction \n3 - Multiplication \n4 - Division \n\n0 - Exit the program \n\nA: ");
    }
}
