package com.minderaschool.rockPaperScissors;

import java.util.concurrent.Semaphore;

public class Semaphores {
    public static final Semaphore player1 = new Semaphore(0);
    public static final Semaphore player2 = new Semaphore(0);
    public static final Semaphore referee = new Semaphore(0);
    public static final Semaphore nextRound = new Semaphore(0);
    public static final Semaphore gameOver = new Semaphore(0);
}
