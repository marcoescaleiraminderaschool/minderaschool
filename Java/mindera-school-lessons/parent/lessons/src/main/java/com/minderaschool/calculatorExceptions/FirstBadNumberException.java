package com.minderaschool.calculatorExceptions;

public class FirstBadNumberException extends Exception {
    public String getMessage() {
        return "First number is not a valid number";
    }
}
