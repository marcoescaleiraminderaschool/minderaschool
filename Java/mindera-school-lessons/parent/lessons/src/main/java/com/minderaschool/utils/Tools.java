package com.minderaschool.utils;

import com.minderaschool.contactList.*;

import java.util.*;

public class Tools {
    static List<Contact> contacts1 = new ArrayList<>() {{
       add(new Contact("c1", "911277"));
       add(new Contact("c2", "458485"));
       add(new Contact("c3", "435234"));
       add(new Contact("c4", "983245"));
    }};
    static List<Contact> contacts2 = new ArrayList<>() {{
        add(new Contact("c5", "983245"));
    }};

    static List<Person> persons = new ArrayList<>() {{
       add(new Person("marco", contacts1));
       add(new Person("marcelo", contacts1));
       add(new Person("andre", contacts1));
       add(new Person("marco", contacts2));
    }};

    public static void main(String[] args) {
        System.out.println("Before merge:");
        printPerson(persons);

        System.out.println();

        List<Person> newList = merge(persons);
        System.out.println("After merge:");
        printPerson(newList);
    }

    static List<Person> merge(List<Person> persons) {
        HashMap<String, Person> map = new HashMap<>();

        for (Person person : persons) {
            String personName = person.getName();
            if (!map.containsKey(personName)) {
                map.put(personName, person);
            } else {
                Person p = map.get(personName);
                List<Contact> contacts = new ArrayList<>();

                contacts.addAll(p.getContacts());

                for (Contact pCon : person.getContacts()) {
                    boolean add = true;
                    for (Contact con : contacts) {
                        if (pCon.getType().equals(con.getType()) && pCon.getValue().equals(con.getValue())) {
                            add = false;
                            break;
                        }
                    }
                    if (add) {
                        contacts.add(pCon);
                    }
                }
                map.replace(personName, new Person(personName, contacts));
            }
        }

        return new ArrayList<>(map.values());
    }


    public static void printPerson(List<Person> list) {
        for (Person person : list) {
            System.out.print(person.getName() + " - ");
            for ( Contact contact : person.getContacts())
                System.out.print(contact.getType() + " - " + contact.getValue() + " | ");
            System.out.println();
        }
    }
}
