package com.minderaschool.exercicios;

import java.util.Arrays;

public class Anagrams {
    public static void main(String[] args) {
        String string1 = "Listen";
        String string2 = "Silent";

        System.out.println(checkAnagrams(string1, string2));
    }

    private static boolean checkAnagrams(String str1, String str2) {
        boolean output = false;

        if (str1.length() == str2.length()) {
            //Handle the strings
            str1 = str1.toLowerCase().trim();
            str2 = str2.toLowerCase().trim();

            //To char array
            char[] chr1 = str1.toCharArray();
            char[] chr2 = str2.toCharArray();
            //Sort char array
            Arrays.sort(chr1);
            Arrays.sort(chr2);

            output = Arrays.equals(chr1, chr2);
        }

        return output;
    }
}
