package com.minderaschool.enumMonths;

/*
* enumMonths
* Create an enumerate with the months of a year, and based on the enumerate value, print the number of days in that month
*/
public class EnumMonths {

    enum Months {
        JANUARY(31),
        FEBRUARY(28),
        MARCH(31),
        APRIL(30),
        MAY(31),
        JUNE(30),
        JULY(31),
        AUGUST(31),
        SEPTEMBER(30),
        OCTOBER(31),
        NOVEMBER(30),
        DECEMBER(31);

        private int numberOfDays;

        private Months(int numberOfDays) {
            this.numberOfDays = numberOfDays;
        }

        public int getNumberOfDays() {
            return numberOfDays;
        }
    }

    public static void main(String[] args) {
        Months month = Months.FEBRUARY;

        System.out.println(month.getNumberOfDays() + " " + month.name());
    }
}
