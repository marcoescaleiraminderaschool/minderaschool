package com.minderaschool.tree;

public class Tree {
    private Node root;

    public Tree() {
        root = null;
    }

    public Node getRoot() {
        return root;
    }

    public Node insertStart(int value) {
        root = new Node(value);
        return root;
    }

    public Node insertLeft(Node node, int value) { //Insert left
        if (node.getLeft() == null) {
            node.setLeft(new Node(value));
        } else {
            insertLeft(node.getLeft(), value);
        }
        return node.getLeft();
    }

    public Node insertRight(Node node, int value) { //Insert right
        if (node.getRight() == null) {
            node.setRight(new Node(value));
        } else {
            insertRight(node.getRight(), value);
        }
        return node.getRight();
    }

    public void preOrder(Node node) {
        System.out.print(node.getValue() + " ");

        if (node.getLeft() != null) {
            this.preOrder(node.getLeft());
        }

        if (node.getRight() != null) {
            this.preOrder(node.getRight());
        }
    }

    public void inOrder(Node node) {
        if (node.getLeft() != null) {
            this.inOrder(node.getLeft());
        }

        System.out.print(node.getValue() + " ");

        if (node.getRight() != null) {
            this.inOrder(node.getRight());
        }
    }

    public void postOrder(Node node) {
        if (node.getLeft() != null) {
            this.postOrder(node.getLeft());
        }

        if (node.getRight() != null) {
            this.postOrder(node.getRight());
        }

        System.out.print(node.getValue() + " ");
    }
}
