package com.minderaschool.fibonacciThreads;

import java.util.HashMap;
import java.util.Map;

class Fibonacci {

    Map<Long, Long> memoization;

    public Fibonacci() {
        this.memoization = new HashMap<>();
    }

    Long fibonacci(Long n) throws IllegalArgumentException, InterruptedException {
        if (n < 0) {
            throw new IllegalArgumentException();
        }
        if (n <= 1) {
            Long firstFib = 1L;
            memoize(0L, firstFib);
            memoize(1L, firstFib);
            return firstFib;
        }
        if (!memoization.containsKey(n)) {
            Runnable r1 = () -> {
                Long fib = 0L;
                Long o1 = memoization.get(n - 1);
                Long o2 = memoization.get(n - 2);

                try {
                    if (o1 != null && o2 != null) {
                        fib = o1 + o2;
                    } else if (o2 != null) { // Doesn't contain n-1
                        fib = fibonacci(n - 1) + o2;
                    } else {
                        fib = fibonacci(n - 1) + fibonacci(n - 2);
                    }
                } catch (InterruptedException e) { e.printStackTrace(); }

                memoize(n, fib);
            };

            Thread t = new Thread(r1);
            t.start();
            t.join();
        }
        return memoization.get(n);
    }

    private synchronized void memoize(Long n, Long fib) {
        memoization.put(n, fib);
    }
}