package com.minderaschool.fourInLineSaveGame;

import java.io.*;


public class GameFile {
    private File file = new File("fourInLineRecord.bin");

    public String getFilePath() { return this.file.getAbsolutePath(); }

    public GameFile() {
        createFile();
    }

    private void createFile() {
        try {
            this.file.createNewFile();
        } catch(IOException e) { System.out.println(e); }
    }

    public boolean saveGame(int[][] array) {
        try {
            ObjectOutputStream writeStream = new ObjectOutputStream(new FileOutputStream(this.file.getAbsolutePath()));

            writeStream.writeObject(array);

            writeStream.close();

            System.out.println("Saving game...");

            return true;
        } catch (IOException e) { System.out.println("Error - " + e); }

        return false;
    }

    public int[][] loadGame() {
        try {
            ObjectInputStream readStream = new ObjectInputStream(new FileInputStream(this.file.getAbsolutePath()));
            int[][] array = (int[][]) readStream.readObject();

            return array;
        } catch (IOException e) { System.out.println("Error - " + e); }
        catch (ClassNotFoundException e) { System.out.println("Error - " + e); }

        return null;
    }

    /*
    public static void deleteFile() {
        Scanner input = new Scanner(System.in);

        System.out.print("Are u sure you want to remove the file? (y/n)\nAnswer: ");
        char opt = input.next().charAt(0);

        if (opt == 'y')
            file.remove();
    }
    */
}
