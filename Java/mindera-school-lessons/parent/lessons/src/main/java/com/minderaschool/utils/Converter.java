package com.minderaschool.utils;

public class Converter {

        public static int value(char num) {
            if (num == 'I') return 1;
            if (num == 'V') return 5;
            if (num == 'X') return 10;
            if (num == 'L') return 50;
            if (num == 'C') return 100;
            if (num == 'D') return 500;
            if (num == 'M') return 1000;

            return 0;
        }


        public static int romanToDecimal(String str) {
            int sum = 0;

            for (int i = 0; i < str.length(); i++)
            {
                int num1 = value(str.charAt(i));

                if (i + 1 < str.length()) {
                    int num2 = value(str.charAt(i+1));

                    if (num1 >= num2)
                        sum += num1;
                    else {
                        sum += num2 - num1;
                        i++;
                    }
                } else {
                    sum = sum + num1;
                    i++;
                }
            }

            return sum;
        }
}
