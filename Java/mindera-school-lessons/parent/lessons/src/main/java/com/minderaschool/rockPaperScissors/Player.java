package com.minderaschool.rockPaperScissors;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;

public class Player implements Runnable {
    private int player;
    private ConcurrentLinkedQueue<Rps> results;

    public Player(int player) {
        this.player = player;
        this.results = new ConcurrentLinkedQueue<>();
    }

    public ConcurrentLinkedQueue<Rps> getResults() { return results; }

    @Override
    public void run() {
        int min = 0, max = 3;
        int choice;

        switch (this.player) {
            case 1:
                try {
                    Semaphores.player1.acquire();
                } catch (InterruptedException e) { e.printStackTrace(); }
                break;
            case 2:
                try {
                    Semaphores.player2.acquire();
                } catch (InterruptedException e) { e.printStackTrace(); }
                break;
        }

        choice = ThreadLocalRandom.current().nextInt(min, max);
        Rps rps = Rps.values()[choice];

        results.add(rps);

        System.out.print(this.player + " - " + rps + " ");

        Semaphores.referee.release();
    }
}
