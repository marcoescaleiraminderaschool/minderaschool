package com.minderaschool.teste16_11;

import java.util.ArrayList;
import java.util.List;

public class Post {
    private String text;
    private String username;
    private List<String> likes;
    private List<Comment> comments;

    public Post(String text, String username) {
        this.text = text;
        this.username = username;
        likes = new ArrayList<>();
        comments = new ArrayList<>();
    }

    public void like(String user) {
        boolean insert = true;
        for (int i = 0; i < likes.size(); i++) {
            if (likes.get(i).equals(user)) {
                insert = false;
            }
        }
        if (insert) {
            likes.add(user);
        }
    }

    public String[] getLikes() {
        int size = likes.size();
        String[] output = new String[size];

        for (int i = 0; i < size; i++) {
            output[i] = likes.get(i);
        }

        return output;
    }

    public void unlike(String user) {
        likes.remove(user);
    }

    public void comment(String text, String username) {
        comments.add(new Comment(text, username));
    }

    public Comment[] getComments() {
        int size = comments.size();
        Comment[] output = new Comment[size];

        for (int i = 0; i < size; i++) {
            output[i] = comments.get(i);
        }

        return output;
    }

    public void print() {
        System.out.println("---");
        System.out.println("\"" + text + "\" - " + username);
        System.out.println("---");
        System.out.print("Likes: ");
        if (likes.size() == 0) {
            System.out.print("No Likes");
        } else {
            for (int i = 0; i < likes.size(); i++) {
                if (i == likes.size() - 1) {
                    System.out.print(likes.get(i));
                } else {
                    System.out.print(likes.get(i) + ", ");
                }
            }
        }
        System.out.println("\n---");
        if (comments.size() == 0) {
            System.out.println("No comments");
        } else {
            for (Comment cm : getComments()) {
                System.out.println("Comment: \"" + cm.getText() + "\" - " + cm.getUsername());
            }
        }
        System.out.println("---");
    }
}
