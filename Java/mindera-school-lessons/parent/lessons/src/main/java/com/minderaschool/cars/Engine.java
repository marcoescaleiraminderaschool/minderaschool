package com.minderaschool.cars;

public class Engine {
    private int hp;
    private boolean started;

    public Engine(int hp) {
        this.hp = hp;
    }

    public boolean isStarted() {
        return started;
    }

    public void start() {
        this.started = true;
    }

    public void stop() {
        this.started = false;
    }
}
