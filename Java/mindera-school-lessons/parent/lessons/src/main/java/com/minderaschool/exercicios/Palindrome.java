package com.minderaschool.exercicios;

public class Palindrome {
    public static void main(String[] args) {
        String str = "mMABCaCBAMm";

        System.out.println(checkPalindrome(str));
    }

    private static boolean checkPalindrome(String str) {
        str = str.trim();

        char[] charStr = str.toCharArray();
        int size = str.length();

        for (int i = 0, j = size - 1; i < size / 2; i++, j--) {
            if (charStr[i] != charStr[j]) {
                return false;
            }
        }

        return true;
    }
}
