package com.minderaschool.list;

public class LinkedList {

    private Node head;
    private Node tail;
    public int length;

    public LinkedList() {
    }

    public void add(int element) {
        System.out.println("LinkedList: Adding new element " + element);
        Node nodeToBeInserted = new Node(element);

        if(head == null) {
            System.out.println("head is null, initializing head with " + nodeToBeInserted);
            head = nodeToBeInserted;
            tail = head;
        } else {
            tail.nextElement = nodeToBeInserted;
            tail = tail.nextElement;
        }
    }

    // Adding an element without using the tail
    //public void add(int element) {
    //    System.out.println("LinkedList: Adding new element " + element);
    //    Node nodeToBeInserted = new Node(element);

    //    if(head == null) {
    //        System.out.println("head is null, initializing head with " + nodeToBeInserted);
    //        head = nodeToBeInserted;
    //    } else {
    //        Node temporaryNode = head;
    //        System.out.println("temporaryNode is at " + temporaryNode);
    //        while(temporaryNode.nextElement != null) {
    //            temporaryNode = temporaryNode.nextElement;
    //            System.out.println("temporaryNode is at " + temporaryNode);
    //        }

    //        temporaryNode.nextElement = nodeToBeInserted;
    //    }

    //}

    public void add(int[] elements) {
        // for(int i = 0; i < elements.length; i++)
        //     this.add(elements[i]);
        // }
        for(int element : elements) {
            this.add(element);
        }
    }

    public void remove(int index) {
        Node temporaryNode = head;

        for (int i = 0; i < index - 1; i ++) {
            temporaryNode = temporaryNode.nextElement;
        }
        temporaryNode.nextElement = temporaryNode.nextElement.nextElement;
    }

    public int get(int index) {
        Node temporaryNode = head;

        int i = 0;
        while(temporaryNode != null && i < index) {
            temporaryNode = temporaryNode.nextElement;
            i++;
        }

        return temporaryNode.element;
    }

    public void clear() {
        head = null;
    }

    public int size() {
        Node temporary = head;
        int size = 0;

        while(temporary != null) {
            size++;
            temporary = temporary.nextElement;
        }

        return size;
    }


    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        int[] numbers = new int[]{50, 60, 70};

        System.out.println("list has " + list.size() + " elements.");
        list.add(10);
        System.out.println("list has " + list.size() + " elements.");
        list.add(new int[]{20, 30, 40});
        list.add(numbers);
        System.out.println("list has " + list.size() + " elements.");

        System.out.println(list.get(6 ));
        list.remove(6);
        System.out.println("list has " + list.size() + " elements.");
        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));
        System.out.println(list.get(4));
        System.out.println(list.get(5));

        System.out.println("Clear list");
        list.clear();
        System.out.println("list has " + list.size() + " elements.");
    }

    private static class Node {
        int element;
        Node nextElement;

        Node() {
        }

        Node(int element) {
            this.element = element;
        }

        public String toString() {
            return "Node: [ element: " + element + ", nextElement: " + nextElement + "]";
        }

    }
}


// 3 -> 5 -> 7 -> 9
