package com.minderaschool.sorter;

import java.util.Comparator;

class Sort {

    int[] integerArray(int[] toSort) {
        return integerArray(toSort, SortOrder.ASCENDING);
    }

    int[] integerArray(int[] toSort, SortOrder sortOrder) {
        if (checkMinimumLength(toSort.length)) {
            return toSort;
        }

        switch (sortOrder) {
            case ASCENDING:
                bubbleSortArray(toSort, this::sortOrderComparator);
                break;
            case DESCENDING:
                bubbleSortArray(toSort, this::sortOrderComparator);
        }

        return toSort;
    }

    private int sortOrderComparator(int o1, int o2) {
        return Integer.compare(o1, o2);
    }

    private boolean checkMinimumLength(int n) {
        return n == 0 || n == 1;
    }

    private void bubbleSortArray(int[] toSort, Comparator<Integer> comparatorFunction) {
        boolean swapped = true;

        while (swapped) {
            for (int i = 0; i < toSort.length - 1; i++) {
                swapped = false;
                for (int j = 0; j < toSort.length - i - 1; j++) {
                    if (comparatorFunction.compare(toSort[j], toSort[j + 1]) > 0) {
                        int temp = toSort[j];
                        toSort[j] = toSort[j + 1];
                        toSort[j + 1] = temp;
                        swapped = true;
                    }
                }
            }
        }
    }
}
