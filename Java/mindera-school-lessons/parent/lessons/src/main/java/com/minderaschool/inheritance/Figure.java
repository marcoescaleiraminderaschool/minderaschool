package com.minderaschool.inheritance;

public class Figure {
    double areaValue = 0.0;
    String whoAmI = "Figure";

    public double area() {
        return this.areaValue;
    }

    public void whoAmI() {
        System.out.println(whoAmI);
    }
}
