package com.minderaschool.sumArray;

/*
* Given nums = [2, 7, 11, 15], target = 9,
* Because nums[0] + nums[1] = 2 + 7 = 9,
* return [0, 1].
*/

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SumArray {
    public static void main(String[] args) {
        int[] nums = new int[]{2, 7, 11, 15};
        int target = 18;

        System.out.println(Arrays.toString(indices(nums, target)));
    }

    private static int[] indices(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], i);
        }
        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (map.containsKey(complement) && map.get(complement) != i) {
                return new int[] { i, map.get(complement) };
            }
        }
        throw new IllegalArgumentException("No two sum solution");
    }
}
