package com.minderaschool.fourInLineSaveGame;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PlayFour {
    public static int MAXROWS    = 7;
    public static int MAXCOLUMNS = 6;
    public static boolean WIN    = false;

    public void menu(boolean status, GameFile fileGame, int[][] board) {
        int player = 1;
        int[][] grid;

        if (board == null) {
            grid = new int[MAXROWS][MAXCOLUMNS]; //y x
        } else {
            grid = board;
        }

        int[] pos;

        if (status) {
            System.out.println("\nPlaying a new game\n");
        } else {
            System.out.println("\nPlaying saved game\n");
        }

        do {
            this.print(grid);

            pos = this.add(grid, player);

            if (pos == null) {
                if (fileGame.saveGame(grid)) {
                    System.out.println("Game saved successfully!");
                }
                break;
            }

            if (player == 1 && !(WIN = this.checkWIN(grid, player, pos[1], pos[0]))){
                player = 2;
            } else {
                player = 1;
            }

        } while(!WIN);

        if (WIN) {
            System.out.print("\nCongratulation ");

            if (player == 1){
                System.out.print("\u001B[31m PLAYER 1");
            } else {
                System.out.print("\u001B[34m PLAYER 2");
            }
            // 0- CLEAR   1- PLAYER1   2- PLAYER2
        }
    }

    public void print(int[][] grid){
        for (int y = 0; y < grid.length; y++) {
            for(int x = 0;x < grid[1].length;x++)
                if (grid[y][x] == 1)
                    System.out.print("\u001B[31m 0 \u001B[0m|");
                else if (grid[y][x] == 2)
                    System.out.print("\u001B[34m 0 \u001B[0m|");
                else
                    System.out.print("\u001B[0m 0 |");

            System.out.println();
        }
    }

    public int[] add(int[][] grid,  int player){
        int column = read(grid, player);
        if (column == -1) {
            return null;
        }

        int row         = -1;
        boolean changed = false;
        int[] positions = new int[2];

        for(int y = grid[1].length; y >= 0 && !changed; y--) {
            if (grid[y][column] == 0){
                grid[y][column] = player;
                row     = y;
                changed = true;
            }
        }

        positions[0] = row; positions[1] = column;
        return positions;
    }

    public int read(int[][] grid, int player){
        int x = 0;
        Scanner scanner       = new Scanner(System.in);
        boolean continueInput = true;

        do {
            do {
                try{
                    System.out.print("PLAYER " + player + ": ");

                    x = scanner.nextInt() - 1;
                    if (x == -1)
                        return -1;
                    continueInput = false;
                } catch (InputMismatchException ex) {
                    System.out.println("\nTry again. (Incorrect input: an integer is required in a range of 1 to 6)");
                    scanner.next();
                    continueInput=true;
                }
            } while (continueInput);
            /*while (scanner.hasNext() && continueInput) {
                System.out.print("PLAYER " + player + ": ");
                if (scanner.hasNextInt()) {
                    x = scanner.nextInt() - 1;
                    continueInput = false;
                } else {
                    System.out.println("\nTry again. (Incorrect input: an integer is required in a range of 1 to 6)");

                    continueInput = true;
                }
            }*/

        } while(checkFull(grid, x));

        return x;
    }

    public boolean checkFull(int[][] grid, int column) { return grid[0][column] != 0; }



    public boolean checkWIN(int[][] grid, int player, int column, int row) {
        // System.out.println(column);
        // Escrever a board no ficheiro aquando da vitoria do player
        if (checkRow(grid, player))
            return true;

        if (checkColumn(grid, player))
            return true;

        if(checkMajorDiagonal(grid, player, column, row))
            return true;

        if(checkMinorDiagonal(grid, player, column, row))
            return true;

        return false;
    }

    private boolean checkRow(int[][]grid, int player) {
        for (int y = 6; y > 2; y--)
            for(int x = 0; x < grid[1].length; x++)
                if (grid[y][x] == player && grid[y-1][x] == player && grid[y-2][x] == player && grid[y-3][x] == player) // HORIZONTAL
                    return true;

        return false;
    }

    private boolean checkColumn(int[][]grid, int player) {
        for (int y = 0; y < grid.length; y++)
            for (int x = 0; x < 3; x++)
                if (grid[y][x] == player && grid[y][x+1] == player && grid[y][x+2] == player && grid[y][x+3] == player) // VERTICAL
                    return true;

        return false;
    }

    private boolean checkMajorDiagonal (int[][]grid, int player, int column, int row) {
        int count = 1;

        //check down major diagonal
        for(int x = column + 1, y = row + 1; y < grid.length && x < grid[0].length; x++, y++) {
            if (grid[y][x] == player)
                count ++;
            else
                break;
        }

        if(count >= 4)
            return true;


        //check up major diagonal
        for(int x = column - 1, y = row - 1; y >= 0 && x >= 0; x--, y--) {
            if (grid[y][x] == player)
                count ++;
            else
                break;
        }

        if(count >= 4)
            return true;

        return false;
    }

    private boolean checkMinorDiagonal (int[][]grid, int player, int column, int row) {
        int count = 1;

        //check down minor diagonal
        for(int x = column - 1, y = row + 1; y < grid.length && x >= 0; x--, y++) {
            if (grid[y][x] == player)
                count ++;
            else
                break;
        }

        if(count >= 4)
            return true;

        //check up minor diagonal
        for(int x = column + 1, y = row - 1; y >= 0 && x < grid[0].length; x++, y--) {
            if (grid[y][x] == player)
                count ++;
            else
                break;
        }

        if(count >= 4)
            return true;

        return false;
    }
}
