package com.minderaschool.cars;

public class Body {
    Door door[];

    public Body(int numDoor) {
        this.door = new Door[numDoor];
        for(int i = 0; i < numDoor; i++)
            this.door[i] = new Door(i);
    }

    public void openDoor(int doorNum) {
        door[doorNum].open();
    }

    public void closeDoor(int doorNum) {
        door[doorNum].close();
    }
}
