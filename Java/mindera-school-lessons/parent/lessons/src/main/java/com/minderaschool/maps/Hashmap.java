package com.minderaschool.maps;

import java.util.*;
/*
Map<String, Integer> sorted = mapa
        .entrySet()
        .stream()
        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
        .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (k, v) -> v, LinkedHashMap::new));

sorted.forEach((k,v) -> System.out.mapCharPrintln(k + " - " + v));
*/

public class Hashmap {
    public static void main(String[] args) {
        String[] words = new String[]{"marco", "marco", "andre", "marcelo", "marcelo", "marco", "miguel", "miguel", "marco", "marcelo"};

        HashMap<String, Integer> map = new HashMap<>(); //Create the map

        for (String word : words) {
            if (!map.containsKey(word)) {
                map.put(word, 1);
            } else {
                map.put(word, map.get(word)+1);
            }
        }

        map.forEach((k,v)->System.out.println(k + " - " + v));
        System.out.println();

        //See the key that shows more times
        int maxValueInMap=(Collections.max(map.values()));      // This will return max value in the Hashmap
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue()==maxValueInMap)
                System.out.println(entry.getKey() + "\n");
        }

        List<Map.Entry<String, Integer>> orderedList = new LinkedList<>(map.entrySet());
        Collections.sort(orderedList, Collections.reverseOrder(new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        }));
        for (Map.Entry<String, Integer> item : orderedList) {
            System.out.println(item);
        }

    }

}
