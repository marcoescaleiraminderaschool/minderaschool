package com.minderaschool.myLinkedList;

public class Main {
    public static void main(String[] args) {
        MyLinkedList linkedList = new MyLinkedList();

        linkedList.addAtHead(1);
        linkedList.addAtTail(3);
        linkedList.addAtTail(1);
        linkedList.addAtTail(3);
        linkedList.addAtTail(1);

        linkedList.print();

        System.out.println(linkedList.isPalindrome());
    }
}
