package com.minderaschool.inheritance;

public class Triangle extends Figure {

    public Triangle(double base, double height) {
        super.whoAmI    = "Triangle";
        super.areaValue = (base * height) / 2;
    }

    public double area() {
        return super.areaValue;
    }

    public void whoAmI() {
        System.out.println(super.whoAmI);
    }
}
