package com.minderaschool.calculatorExceptions;

public class SyntaxCalculateException extends Exception {
    public String getMessage() {
        return "Bad syntax. Example: 1 + 1";
    }
}
