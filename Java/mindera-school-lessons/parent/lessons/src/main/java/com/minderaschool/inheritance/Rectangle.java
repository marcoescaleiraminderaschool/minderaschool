package com.minderaschool.inheritance;

public class Rectangle extends Figure {
    public Rectangle() {
    }

    public Rectangle(double largeSide, double smallSide) {
        super.whoAmI = "Rectangle";
        super.areaValue = largeSide*smallSide;
    }

    public double area() {
        return super.areaValue;
    }

    public void whoAmI() {
        System.out.println(super.whoAmI);
    }
}
