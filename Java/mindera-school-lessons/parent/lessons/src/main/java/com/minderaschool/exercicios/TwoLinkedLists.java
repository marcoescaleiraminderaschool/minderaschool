package com.minderaschool.exercicios;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class TwoLinkedLists {
    public static void main(String[] args) {
        LinkedList<Integer> linkedList1 = new LinkedList<>() {{
            add(1);
            add(2);
            add(3);
        }};
        LinkedList<Integer> linkedList2 = new LinkedList<>() {{
            add(4);
            add(5);
            add(6);
        }};

        Stack<Integer> stack = new Stack<>();

        for (int i = linkedList1.size() - 1; i >= 0; i--) {
            stack.push(linkedList1.get(i)+linkedList2.get(i));
        }

        while(!stack.isEmpty()) {
            System.out.print(stack.pop() + " ");
        }
    }
}
