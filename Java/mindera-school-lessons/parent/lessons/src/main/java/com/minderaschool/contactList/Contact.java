package com.minderaschool.contactList;

public class Contact {
    private String type;
    private String value;

    public Contact(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public String getType() { return type; }

    public String getValue() { return value; }
}
