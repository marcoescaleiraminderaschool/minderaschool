package com.minderaschool.cars;

public class Car {

    Engine engine;
    Tire tire[];
    Body body;


    public Car(int hp, int doorNum, int tireNums) {
        this.engine = new Engine(hp);

        this.tire   = new Tire[tireNums];
        for(int i = 0; i < tireNums; i++)
            this.tire[i] = new Tire();
        this.body   = new Body(doorNum);
    }

    public void move() {
        if (!engine.isStarted())
            engine.start();

        for(Tire tire : tire)
            tire.rotate();

    }

    public void openDoor(int doorNum) {
        body.openDoor(doorNum);
    }

    public void closeDoor(int doorNum) {
        body.closeDoor(doorNum);
    }
}
