package com.minderaschool.calculatorExceptions;

import java.util.Arrays;

public class Calculator {
    public float calculate (String calc) throws
            FirstBadNumberException,
            SecondBadNumberException,
            SyntaxCalculateException,
            BadOperatorException,
            DivideByZeroException {
        String[] parts = calc.trim().split(" ");

        if (parts.length < 3 || parts.length > 3) {
            throw new SyntaxCalculateException();
        }

        try {
            Float.parseFloat(parts[0]);
        } catch (Exception e) {
            throw new FirstBadNumberException();
        }

        try {
            Float.parseFloat(parts[2]);
        } catch (Exception e) {
            throw new SecondBadNumberException();
        }

        float firstNumber = Float.parseFloat(parts[0]);
        String operator = parts[1];
        float secondNumber = Float.parseFloat(parts[2]);

        switch (operator) {
            case "+":
                return firstNumber + secondNumber;
            case "-":
                return firstNumber - secondNumber;
            case "*":
                return firstNumber - secondNumber;
            case "/":
                if (secondNumber == 0) {
                    throw new DivideByZeroException();
                }
                return firstNumber / secondNumber;
        }
        throw new BadOperatorException();
    }
}
