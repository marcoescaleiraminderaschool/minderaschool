package com.minderaschool.myLinkedList;

public class MyLinkedList {
    Node start;
    Node end;
    private int size;

    public MyLinkedList() {
        this.start = null;
        this.end = null;
        this.size = 0;
    }

    public boolean isEmpty() {
        return start == null;
    }

    public int size() {
        return this.size;
    }

    //Add a node of value val before the first element of the linked list.
    // After the insertion, the new node will be the first node of the linked list.
    public void addAtHead(int val) {
        Node node = new Node(val, null);

        if (start == null) {
            start = node;
            end = start;
        } else {
            node.setNext(start);
            start = node;
        }
        size ++;
    }

    //addAtTail(val) : Append a node of value val to the last element of the linked list.
    public void addAtTail(int val) {
        Node nodeToInsert = new Node(val, null);
        if (start == null) {
            start = nodeToInsert;
            end = start;
        } else {
            end.setNext(nodeToInsert);
            end = nodeToInsert;
        }
        size++;
    }

    //addAtIndex(index, val) : Add a node of value val before the index-th node in the linked list.
    // If index equals to the length of linked list, the node will be appended to the end of linked list.
    // If index is greater than the length, the node will not be inserted.
    public void addAtIndex(int index, int val) {
        validateIndex(index);

        Node nodeToInsert = new Node(val, null);

        if (index == 0) {
            addAtHead(val);
            return;
        }
        if (index == size) {
            addAtTail(val);
            return;
        }

        Node node = start;
        for (int i = 1; i <= size; i++) {
            if (i == index) {
                Node tmp = node.getNext();
                node.setNext(nodeToInsert);
                nodeToInsert.setNext(tmp);
            }
            node = node.getNext();
        }
        size++;
    }

    //get(index) : Get the value of the index-th node in the linked list. If the index is invalid, return -1.
    private Node getNode(int index) {
        validateIndex(index);

        Node node = start;

        for (int i = 0; node != null && i < index; i++) {
            node = node.getNext();
        }

        return node;
    }

    public int get(int index) {
        return getNode(index).getElement();
    }

    //deleteAtIndex(index) : Delete the index-th node in the linked list, if the index is valid.
    public void deleteAtIndex(int index) {
        validateIndex(index);

        if (index == 0) {
            start = start.getNext();
            return;
        }

        Node parent = getNode(index - 1);
        parent.next = parent.next.next;

        size--;
    }

    public boolean isPalindrome() {
        MyLinkedList reversedList = reverse();
        return equals(reversedList);
    }

    private boolean equals(MyLinkedList list) {
        if (list.size != this.size) {
            return false;
        }

        Node current = start;
        Node otherCurrent = list.start;

        do {
            if (current.getElement() != otherCurrent.getElement()) {
                return false;
            }
            current = current.getNext();
            otherCurrent = otherCurrent.getNext();
        } while (current.getNext() != null);
        return true;
    }

    private MyLinkedList reverse() {
        MyLinkedList reversedList = new MyLinkedList();
        reversedList.addAtHead(start.getElement());
        Node ptr = start;
        while (ptr.getNext() != null) {
            ptr = ptr.getNext();
            reversedList.addAtHead(ptr.getElement());
        }

        return reversedList;
    }

    public void print() {
        System.out.print("\nLinked List = ");
        if (size == 0) {
            System.out.print("Empty\n");
            return;
        }
        if (start.getNext() == null) {
            System.out.println(start.getElement());
            return;
        }
        Node ptr = start;
        System.out.print(start.getElement() + " -> ");
        ptr = start.getNext();
        while (ptr.getNext() != null) {
            System.out.print(ptr.getElement() + " -> ");
            ptr = ptr.getNext();
        }
        System.out.print(ptr.getElement() + "\n");
        System.out.println();
    }

    private void validateIndex(int index) {
        if (index < 0 || index > this.size) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }
}
