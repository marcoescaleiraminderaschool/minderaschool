package com.minderaschool.cars;

import com.minderaschool.utils.Logger;

public class Door {

    private int doorNum;
    private boolean opened;

    private static final Logger LOG = new Logger("Door");

    public Door(int doorNum) {
        this.doorNum = doorNum;
    }

    public boolean isOpened() { return opened; }

    public void open() {
        this.opened = true;
        LOG.info("door " + this.doorNum + " has been opened");
    }

    public void close() {
        this.opened = false;
        LOG.info("door " + this.doorNum + " has been closed");
    }
}
