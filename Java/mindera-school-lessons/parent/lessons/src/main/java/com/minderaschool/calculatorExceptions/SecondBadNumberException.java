package com.minderaschool.calculatorExceptions;

public class SecondBadNumberException extends Exception {
    public String getMessage() {
        return "Second number is not a valid number";
    }
}
