package com.minderaschool.inheritance;

public class Circle extends Figure {

    public Circle(double radius) {
        super.whoAmI = "Circle";
        super.areaValue = Math.PI * (radius * radius);
    }

    public double area() {
        return super.areaValue;
    }

    public void whoAmI() {
        System.out.println(super.whoAmI);
    }
}
