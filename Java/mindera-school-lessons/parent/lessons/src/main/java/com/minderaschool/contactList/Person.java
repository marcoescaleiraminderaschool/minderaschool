package com.minderaschool.contactList;

import java.util.HashMap;
import java.util.List;

public class Person {
    private String name;
    private List<Contact> contacts;

    public Person(String name, List<Contact> contacts) {
        this.name = name;
        this.contacts = contacts;
    }

    public String getName() { return name; }

    public List<Contact> getContacts() { return contacts; }

}
