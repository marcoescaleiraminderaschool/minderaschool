package com.minderaschool.exercicios;

import java.util.LinkedHashMap;
import java.util.Map;

/*
for (int i = 0; i < string.length(); i++) {
    boolean check = true;
    char iChar = string.charAt(i);
    for (int j = i + 1; j < string.length(); j++) {
        char jChar = string.charAt(j);
        if (iChar == jChar) {
            check = false;
            break;
        }
    }
    if (check) {
        System.out.println(string.charAt(i));
    }
}
 */
public class FirstNonRepeatedChar {
    public static void main(String[] args) {
        String string  = "simplest";    // -> i
        String string2 = "abcdefghija"; // -> b

        System.out.println(getFirstNonRepeated(string));
        System.out.println(getFirstNonRepeated(string2));
    }

    public static char getFirstNonRepeated(String string) {
        if (string.length() > 0) {
            LinkedHashMap<Character, Integer> map = new LinkedHashMap<>();

            for (int i = 0; i < string.length(); i++) {
                char iChar = string.charAt(i);

                if (map.containsKey(iChar)) {
                    map.put(iChar, map.get(string.charAt(i)) + 1);
                } else {
                    map.put(iChar, 1);
                }
            }

            for (Map.Entry<Character, Integer> entry : map.entrySet()) {
                if (entry.getValue() == 1) {
                    return entry.getKey();
                }
            }
        }

        return ' ';
    }
}
