package com.minderaschool;

public class SumNumbers {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5, 6};
        System.out.println(sumEvenNumbers(numbers));
    }

    static int sumEvenNumbers (int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                sum += numbers[i];
            }
        }
        return sum;
    }

    static int[] getChange (double price, double received) {

    }

}
