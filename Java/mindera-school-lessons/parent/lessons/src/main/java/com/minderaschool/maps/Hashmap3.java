package com.minderaschool.maps;

import java.util.*;

public class Hashmap3 {
    public static void main(String[] args) {
        listOccurrences("Hello World");
    }

    public static void listOccurrences(String string) {
        char[] word = string.replaceAll(" ", "").toCharArray();
        HashMap<Character, Integer[]> map = new HashMap<>();
        for (int i = 0; i < word.length; i++) {
            char letter = word[i];

            if (map.containsKey(letter)) {
                for (Map.Entry entry : map.entrySet()) {
                    if (entry.getKey().equals(letter)) {
                        Integer[] array = getInteger((Integer[]) entry.getValue(), i);
                        map.put(letter, array);
                        break;
                    }
                }
            } else {
                map.put(word[i], new Integer[]{i});
            }
        }
        map.forEach((k, v) -> {
            System.out.print(k + "=[");
            for (int i = 0; i < v.length; i++) {
                System.out.print(v[i]);
                if (i != v.length - 1) { //check to see if its last number
                    System.out.print(",");
                }
            }
            System.out.print("] ");
        });

        System.out.println();
        printOrder(map, word);
    }

    public static void printOrder(HashMap<Character, Integer[]> map, char[] word) {
        boolean[] found = new boolean[256];
        StringBuilder sb = new StringBuilder();
        for (char c : word) {
            if (!found[c]) {
                found[c] = true;
                sb.append(c); //+=
            }
        }
        word = sb.toString().toCharArray();
        for (char letter : word) {
            for (Map.Entry entry : map.entrySet()) {
                if (entry.getKey().equals(letter)) {
                    System.out.print(entry.getKey() + "=[");
                    Integer[] v = (Integer[]) entry.getValue();
                    for (int i = 0; i < v.length; i++) {
                        System.out.print(v[i]);
                        if (i != v.length - 1) {
                            System.out.print(",");
                        }
                    }
                    System.out.print("] ");
                }
            }
        }
    }

    public static Integer[] getInteger(Integer[] array, int num) {
        Integer[] newArray = new Integer[array.length + 1];
        int i;
        for (i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        newArray[i] = num;
        return newArray;
    }
}
