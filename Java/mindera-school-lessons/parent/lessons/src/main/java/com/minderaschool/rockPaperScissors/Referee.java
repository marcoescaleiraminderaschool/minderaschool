package com.minderaschool.rockPaperScissors;

public class Referee implements Runnable {

    private Player p1, p2;
    private int p1Wins, p2Wins, draws;
    private int bestOf;

    public Referee(Player p1, Player p2, int bestOf) {
        this.p1 = p1;
        this.p2 = p2;
        this.bestOf = bestOf;
    }

    public int getP1Wins() {
        return p1Wins;
    }

    public int getP2Wins() {
        return p2Wins;
    }

    @Override
    public void run() {
        try {
            nextRound();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void checkGameWinner() {
        System.out.println("\n| Match result |");
        if (p1Wins == p2Wins) {
            System.out.println("DRAW");
        } else if (p1Wins > p2Wins) {
            System.out.println("Player 1 wins");
        } else {
            System.out.println("Player 2 wins");
        }
    }

    private void nextRound() throws InterruptedException {
        Semaphores.player1.release(1);
        Semaphores.player2.release(1);

        Semaphores.referee.acquire(2);

        Rps one = p1.getResults().remove();
        Rps two = p2.getResults().remove();

        int winner = checkRoundWinner(one, two);
        System.out.println("\n\nRound winner: " + (winner == 0 ? "DRAW" : "PLAYER " + winner));
        printScoreboard();

        Semaphores.nextRound.release();
    }

    private void printScoreboard() {
        System.out.printf("\nPlayer 1 - %d | Player 2 - %d | Draws - %d\n", p1Wins, p2Wins, draws);
        System.out.println("------------------------------------------");
    }

    private int checkRoundWinner(Rps one, Rps two) {
        // Player 1 wins scenarios
        if (one == Rps.ROCK && two == Rps.SCISSORS ||
                one == Rps.SCISSORS && two == Rps.PAPER ||
                one == Rps.PAPER && two == Rps.ROCK) {
            p1Wins++;
            return 1;

        // Player 2 wins scenarios
        } else if (one == Rps.SCISSORS && two == Rps.ROCK ||
                one == Rps.PAPER && two == Rps.SCISSORS ||
                one == Rps.ROCK && two == Rps.PAPER) {
            p2Wins++;
            return 2;
        }
        // Then it's a draw
        draws++;
        return 0;
    }
}
