package com.minderaschool.doubleLinkedList;

public class Node {
    int element;
    Node next, prev;

    public Node() {
        this.element = 0;
        this.next = null;
        this.prev = null;
    }

    public Node(int element, Node next, Node prev) {
        this.element = element;
        this.next = next;
        this.prev = prev;
    }


    public void setLinkNext(Node nextNode) { //Func to set link to next node
        this.next = nextNode;
    }

    public void setLinkPrev(Node prevNode) { //Func to set link to previous node
        this.prev = prevNode;
    }


    //Getters
    public Node getNext() { return next; }
    public Node getPrev() { return prev; }

    //Getter & Setter for element
    public void setElement(int element) { this.element = element; }
    public int getElement() { return element; }

}
