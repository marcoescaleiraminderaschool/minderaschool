package com.minderaschool.calculatorExceptions;

public class DivideByZeroException extends Exception{
    public String getMessage() {
        return "You can't divide a number by zero";
    }
}
