package com.minderaschool.rockPaperScissors;

public enum Rps {
    ROCK,
    PAPER,
    SCISSORS
}
