package com.minderaschool.metrics;

import java.util.HashMap;

public class Metric {
    private String metricName;
    private String metricValue;
    private String type;
    private HashMap<String,String> labels;

    public Metric(String metricName, String metricValue, String type, HashMap<String, String> labels) {
        this.metricName = metricName;
        this.metricValue = metricValue;
        this.type = type;
        this.labels = new HashMap<>(labels);
    }

    public String getMetricName() {
        return metricName;
    }

    public String getMetricValue() {
        return metricValue;
    }

    public String getType() {
        return type;
    }

    public HashMap<String, String> getLabels() {
        return labels;
    }
}
