package com.minderaschool.restaurant;

import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

public class Restaurant {
    private static final Queue<String> entryQueue = new ConcurrentLinkedQueue<>();
    private static final Semaphore entrySemaphore = new Semaphore(0);

    private static final Queue<String> chefQueue = new ConcurrentLinkedQueue<>();
    private static final Semaphore chefSemaphore = new Semaphore(0);

    private static void changeEntrySemaphore() {
        if (!entryQueue.isEmpty()) {
            entrySemaphore.release(1);
        }
        entrySemaphore.release(0);
    }

    private static void changeChefSemaphore() {
        if (!chefQueue.isEmpty()) {
            chefSemaphore.release(1);
        }
        chefSemaphore.release(0);
    }

    public static void main(String[] args) throws InterruptedException {
        Thread generate = new Thread(() -> {
            for (int i = 0; i < 1; i++) {
                String randomStr = UUID.randomUUID().toString();
                entryQueue.add(randomStr);
                System.out.println("Generating... | Queue: " + entryQueue);

                changeEntrySemaphore();

                try {
                    System.out.println("Generate sleeping");
                    Thread.sleep(2000);
                } catch (InterruptedException e) { e.printStackTrace(); }
            }
        });

        Thread receptionist = new Thread(() -> {
            while (true) {
                try {
                    entrySemaphore.acquire();
                } catch (InterruptedException e) { e.printStackTrace(); }

                String nextPerson = entryQueue.remove();
                chefQueue.add(nextPerson);

                System.out.println("Receptionist transferred client to chef | ChefQueue: " + chefQueue);

                changeEntrySemaphore();
                changeChefSemaphore();

                try {
                    System.out.println("Receptionist sleeping");
                    Thread.sleep(2000);
                } catch (InterruptedException e) { e.printStackTrace(); }
            }
        });

        Thread chef = new Thread(() -> {
            while (true) {
                try {
                    chefSemaphore.acquire();
                } catch (InterruptedException e) { e.printStackTrace(); }

                String removed = chefQueue.remove();
                System.out.println("Chef taking care of the client: " + removed);

                changeChefSemaphore();

                try {
                    System.out.println("Chef sleeping");
                    Thread.sleep(2000);
                } catch (InterruptedException e) { e.printStackTrace(); }
            }
        });

        generate.start();
        receptionist.start();
        chef.start();
    }
}
