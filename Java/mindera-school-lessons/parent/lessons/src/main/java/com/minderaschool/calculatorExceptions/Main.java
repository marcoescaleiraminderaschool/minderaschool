package com.minderaschool.calculatorExceptions;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Scanner sc = new Scanner(System.in);

        try {
            System.out.print("Calculate: ");
            String calc = sc.nextLine();
            float add = calculator.calculate(calc);
            System.out.println(add);
        } catch (SyntaxCalculateException |
                FirstBadNumberException |
                SecondBadNumberException |
                BadOperatorException |
                DivideByZeroException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println("Unknown error");
        }
    }
}
