package com.minderaschool.doubleLinkedList;

import com.minderaschool.utils.TextIO;

public class Main {
    public static void main(String[] args) {
        //Scanner scan = new Scanner(System.in);
        DoublyLinkedList list = new DoublyLinkedList();

        int choice;

        do {
            printMenu();

            choice = TextIO.getLnInt();
            System.out.println();
            switch (choice) {
                case 1:
                    System.out.print("Enter integer element to add: ");
                    list.add( TextIO.getLnInt() );
                    break;
                case 2:
                    System.out.print("Enter position: ");
                    int p = TextIO.getLnInt();
                    if (p < 1 || p > list.size() ) {
                        System.out.println("Invalid position\n");
                    } else {
                        list.remove(p);
                    }
                    break;
                case 3:
                    System.out.println("Enter position: ");
                    int pos = TextIO.getLnInt();
                    System.out.println("Value on position " + pos + ": " + list.getElement(pos));
                    break;
                case 4:
                    System.out.println("Empty status = "+ list.isEmpty());
                    break;
                case 5:
                    System.out.println("Size = "+ list.size() +"\n");
                    break;
                case 0:
                    System.out.println("Leaving... \n");
                    break;
            }

            list.print();
        } while (choice != 0);
    }

    public static void printMenu() {
        System.out.println("Doubly Linked List");
        System.out.println(" 1. Insert");
        System.out.println(" 2. Remove");
        System.out.println(" 3. Get an element");
        System.out.println(" 4. Check if it's empty");
        System.out.println(" 5. Size");
        System.out.println(" 0. Exit");
        System.out.print("Choice: ");
    }
}