package com.minderaschool.metrics;

import com.minderaschool.utils.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Exercise1 {
    private Logger log;
    private File file;
    private FileReader fr;

    public Exercise1() {
        this.log = new Logger();
        this.file = new File("metrics.txt");
        try {
            this.fr = new FileReader(file);
        } catch (FileNotFoundException e) {
            log.error(e.toString());
        }
    }

    public List<Metric> readFile() {
        List<Metric> output = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                output.add(splitMetrics(scanner.nextLine()));
            }
        } catch (IOException e) {
            log.error(e.toString());
        }
        return output;
    }


    public Metric splitMetrics(String line) { // door.open:1|a|#doorNumber:210,whoOpened:Miguel
        String[] splitted = line.split("\\|");

        String[] metricFields = splitted[0].split(":");
        String metricName = metricFields[0]; //Pegar a metric name
        String metricValue = metricFields[1]; //Pegar o value da metric

        String type = splitted[1]; //Pegar o type

        //Pegar as labels
        HashMap<String, String> map = new HashMap<>();
        String[] labels = splitted[2].substring(1).split(",");
        for (String label : labels) {
            String[] dividedLabel = label.split(":");
            map.put(dividedLabel[0], dividedLabel[1]);
        }

        return new Metric(metricName, metricValue, type, map);
    }
}
