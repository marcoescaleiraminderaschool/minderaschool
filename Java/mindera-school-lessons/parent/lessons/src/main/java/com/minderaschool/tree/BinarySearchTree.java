package com.minderaschool.tree;

public class BinarySearchTree {
    Node root;

    public BinarySearchTree() {
        root = null;
    }

    public Node insertNode(int value) {
        return root = new Node(value);
    }

    public void insertNode(Node currentNode, int value) {
        Node leftBranch = currentNode.getLeft();
        Node rightBranch = currentNode.getRight();

        if (value <= currentNode.getValue() && leftBranch != null) {
            insertNode(leftBranch, value);
        } else if (value <= currentNode.getValue()) {
            currentNode.setLeft(new Node(value));
        } else if (value >= currentNode.getValue() && rightBranch != null) {
            insertNode(rightBranch, value);
        } else if (value >= currentNode.getValue()) {
            currentNode.setRight(new Node(value));
        }
    }

    public boolean findNode(Node currentNode, int value) {
        int nodeVal = currentNode.getValue();
        System.out.print(nodeVal + " ");

        if (value < nodeVal && currentNode.getLeft() != null) {
            return findNode(currentNode.getLeft(), value);
        }

        if (value > nodeVal && currentNode.getRight() != null) {
            return findNode(currentNode.getRight(), value);
        }

        return value == nodeVal;
    }
}
