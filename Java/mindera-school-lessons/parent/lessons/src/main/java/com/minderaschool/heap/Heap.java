package com.minderaschool.heap;

public class Heap {
    public static boolean isMinHeap(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            if (!isGreater(arr, i)){
                return false;
            }
        }
        return true;
    }

    public static boolean isMaxHeap(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            if (isGreater(arr, i)){
                return false;
            }
        }
        return true;
    }

    private static boolean isGreater(int[] arr, int i) {
        return arr[(i-1) / 2] < arr[i];
    }
}
