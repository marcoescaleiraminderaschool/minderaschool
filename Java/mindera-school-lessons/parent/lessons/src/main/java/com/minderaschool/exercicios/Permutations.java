package com.minderaschool.exercicios;

public class Permutations {

    public static void main(String[] args) {
        String s = "OBC";

        permutations(s);
    }

    private static void permutations(String str) {
        String output = "";
        permutations("", str, output);
    }

    private static void permutations(String prefix, String suffix) {
        String output = "";
        permutations(prefix, suffix, output);
    }

    private static void permutations(String prefix, String suffix, String results) {
        if (suffix.length() == 0) {
            results = prefix;
            System.out.println(results);
        } else {
            int sSize = suffix.length();
            for (int i = 0; i < sSize; i++) {
                permutations(prefix + suffix.charAt(i), suffix.substring(0, i) + suffix.substring(i + 1, sSize));
            }
        }
    }
}
