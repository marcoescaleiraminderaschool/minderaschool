package com.minderaschool.fibonacci;

import java.util.Arrays;

public class Fibonacci {
    public static long[] fibSeq(int length) {
        if (length == 1) {
            return new long[] {1};
        }
        if (length == 2) {
            return new long[] {1, 1};
        }

        long[] seq = fibSeq(length - 1);
        long[] returnSeq = Arrays.copyOf(seq, length);
        returnSeq[length - 1] = returnSeq[length - 3] + returnSeq[length - 2];

        return returnSeq;
    }
}
