package com.minderaschool.rockPaperScissors;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Brain {
    public Brain() {
    }

    public void play(int bestOf) throws InterruptedException {
        ScheduledExecutorService scheduledThreadPool = new ScheduledThreadPoolExecutor(3);

        Player p1 = new Player(1);
        Player p2 = new Player(2);
        Referee ref = new Referee(p1, p2, bestOf);

        do {
            scheduledThreadPool.schedule(ref, 500, TimeUnit.MILLISECONDS);
            scheduledThreadPool.schedule(p1, 1000, TimeUnit.MILLISECONDS);
            scheduledThreadPool.schedule(p2, 1000, TimeUnit.MILLISECONDS);
            if (ref.getP1Wins() >= bestOf || ref.getP2Wins() >= bestOf) {
                Semaphores.gameOver.release();
            } else {
                Semaphores.nextRound.acquire();
            }
        } while (Semaphores.gameOver.availablePermits() == 0);

        scheduledThreadPool.shutdown();
        ref.checkGameWinner();
    }
}
