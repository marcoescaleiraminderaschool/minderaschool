package com.minderaschool.exercicios;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class FindMissNumber {
    public static void main(String[] args) {
        int[] array = ThreadLocalRandom.current().ints(1, 101).distinct().limit(100).toArray();
        int sum = IntStream.of(array).sum();
        int random = (int)(Math.random() * 100 + 1);
        System.out.println("Random num: " + array[random - 1]);
        array[random - 1] = 0;

        //Find the missing number
        System.out.println(findMissinNumber(array, sum));
    }

    static int findMissinNumber(int[] arr, int sum) {
        int sumOfAllNumbers = IntStream.of(arr).sum(); //Suppose to be 4950
        return (sum - sumOfAllNumbers);
    }
}
