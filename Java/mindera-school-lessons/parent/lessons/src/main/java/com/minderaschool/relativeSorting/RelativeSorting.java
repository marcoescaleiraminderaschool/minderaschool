package com.minderaschool.relativeSorting;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

class RelativeSorting {

    static List<Integer> relativeSorting2(List<Integer> a, List<Integer> o) {
        return a.stream()
                .sorted(relativeSortingComparator(o))
                .collect(toList());
    }

    private static Comparator<Integer> relativeSortingComparator(List<Integer> o) {
        return (o1, o2) -> {
            if (o.contains(o1) && o.contains(o2)) {
                if (o.indexOf(o1) < o.indexOf(o2)) {
                    return -1;
                } else if (o.indexOf(o1) > o.indexOf(o2)) {
                    return 1;
                } else {
                    return 0;
                }
            } else if (o.contains(o1)) {
                return -1;
            } else if (o.contains(o2)) {
                return 1;
            } else {
                return o1.compareTo(o2);
            }
        };
    }

    // 8 9 2 3 7 5 13 9 2 8
    // 9 9 3 13 5 2 2 7 8 8

}
