package com.minderaschool.cars;

import com.minderaschool.utils.Logger;

public class Tire {
    private int rimSize;

    private static final Logger LOG = new Logger("Tire");

    public Tire() {
        this(17);
    }

    public Tire(int rimSize) {
        this.rimSize = rimSize;
    }

    public void rotate() {
        LOG.info("rotating...");
    }
}
