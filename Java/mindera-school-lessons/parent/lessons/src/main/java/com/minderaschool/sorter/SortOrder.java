package com.minderaschool.sorter;

public enum SortOrder {
    ASCENDING,
    DESCENDING
}
