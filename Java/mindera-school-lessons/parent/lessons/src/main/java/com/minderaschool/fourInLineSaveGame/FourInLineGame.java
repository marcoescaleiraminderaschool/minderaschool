package com.minderaschool.fourInLineSaveGame;

import java.util.Date;
import java.util.Scanner;

public class FourInLineGame {

    public static void main(String[] args){
        mainMenu();
    }


    /* Main menu */
    public static void mainMenu() {
        Scanner input = new Scanner(System.in);
        Date date = new Date();

        System.out.print("-------------------------------\n" +
                "1 - Start game\n" +
                "2 - Load game\n" +
                "0 - Exit\n" +
                "Option: ");
        int opt = input.nextInt();

        GameFile fileGame = new GameFile();
        PlayFour game = new PlayFour();

        switch (opt) {
            case 1:
                game.menu(true, fileGame, null);
                break;
            case 2:
                game.menu(false, fileGame, fileGame.loadGame());
                break;
        }

    }
}