package com.minderaschool.metrics;

import com.minderaschool.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static Logger log1 = new Logger("Exercicio 1");
    public static Logger log2 = new Logger("Exercicio 2");
    public static Logger log3 = new Logger("Exercicio 3");

    public static void main(String[] args) {
        Exercise1 ex1 = new Exercise1();

        List<Metric> list = ex1.readFile();

        printList(list);

        averageDoorOpen(list);

        doorOpenTimes(list, "Andre", "210");

        List<Metric> queriedList = queryMetrics(list, "door.open", new HashMap<>() {{ put("doorNumber", "210"); put("whoOpened", "Andre"); }});

        System.out.println("-----------------------------------");
        printList(queriedList);
    }

    public static List<Metric> queryMetrics(List<Metric> metrics, String metricName, Map<String, String> labels) {
        List<Metric> result = new ArrayList<>();

        for (Metric metric : metrics) {
            if (metric.getMetricName().equals(metricName)) {
                int found = 0;
                for (Map.Entry entry : labels.entrySet()) {
                    HashMap<String, String> metricLabels = metric.getLabels();

                    if (metricLabels.containsKey(entry.getKey())) { //Checks if metric contains the labels
                        if (!metricLabels.get(entry.getKey()).equals(entry.getValue())) { // Compares the value
                            found = 0;
                            break;
                        }
                        found++;
                    } else {
                        found = 0;
                        break;
                    }
                }
                if (found == labels.size()) { //If all labels are correct add to the result list
                    result.add(metric);
                }
            }
        }
        return result;
    }

    public static void doorOpenTimes(List<Metric> list, String name, String door) { //With name x check how many times that person opened door x
        int counter = 0;
        for (Metric line : list) {
            HashMap<String, String> labels = line.getLabels();
            if (line.getMetricName().equals("door.open")) {
                if (labels.containsKey("whoOpened")) {
                    if (labels.get("whoOpened").equals(name) && labels.get("doorNumber").equals(door)) {
                        counter++;
                    }
                }
            }
        }
        log2.info(name + " opened " + counter + " times door " + door);
    }

    public static void averageDoorOpen(List<Metric> list) {
        double sum = 0;
        for (Metric line : list) {
            sum += Double.parseDouble(line.getMetricValue());
        }
        log2.info("The average of 'door.open' is: " + (sum / list.size()));
    }

    public static void printList(List<Metric> list) {
        for (Metric line : list) {
            System.out.print("Metric name: " + line.getMetricName());
            System.out.print("\nMetric value: " + line.getMetricValue());
            System.out.print("\nType: " + line.getType());
            System.out.println("\nLabels: ");
            for (Map.Entry entry : line.getLabels().entrySet()) {
                System.out.println("\t" + entry.getKey() + ": " + entry.getValue());
            }
            System.out.println();
            System.out.println("------------");
        }
    }
}