package com.minderaschool.calculatorExceptions;

public class BadOperatorException extends Exception {
    public String getMessage() {
        return "You have entered a non authorized operator";
    }
}
