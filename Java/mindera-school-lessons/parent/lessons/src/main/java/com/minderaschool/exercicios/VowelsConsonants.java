package com.minderaschool.exercicios;

import java.util.HashMap;

// Count a number of vowels and consonants in a given string
public class VowelsConsonants {
    public static final HashMap<Character, Integer> vowels = new HashMap<>() {{
        put('a', 1);
        put('e', 1);
        put('i', 1);
        put('o', 1);
        put('u', 1);
    }};

    public static void main(String[] args) {
        String str = "marco";

        HashMap<String, Integer> map = getVowelsConsonants(str);

        System.out.println(map);
    }

    private static HashMap<String, Integer> getVowelsConsonants(String word) {
        HashMap<String, Integer> outputMap = new HashMap<>();

        int vowelsCounter = 0;
        int consonantsCounter = 0;
        char[] letters = word.toCharArray();

        for (Character character : letters) {
            if (vowels.containsKey(character)) {
                vowelsCounter++;
                continue;
            }
            consonantsCounter++;
        }

        outputMap.put("vowels", vowelsCounter);
        outputMap.put("consonants", consonantsCounter);

        return outputMap;
    }
}
