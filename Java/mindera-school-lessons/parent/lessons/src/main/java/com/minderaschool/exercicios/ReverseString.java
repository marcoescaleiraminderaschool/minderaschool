package com.minderaschool.exercicios;

public class ReverseString {
    public static void main(String[] args) {
        System.out.println(reverseString("marcelo"));
    }

    public static String reverseString(String str) {
        if (str.isEmpty()) {
            return str;
        }
        return reverseString(str.substring(1)) + str.charAt(0);
    }
}
