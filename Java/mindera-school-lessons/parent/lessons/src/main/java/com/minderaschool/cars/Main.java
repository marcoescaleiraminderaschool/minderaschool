package com.minderaschool.cars;

public class Main {

    public static void main(String[] args) {
        Car carro = new Car(180, 4, 4);

        carro.move();

        carro.openDoor(1);
        carro.closeDoor(1);
    }

}
