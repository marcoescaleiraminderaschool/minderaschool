package com.minderaschool.chocolateDistribution;

import java.util.ArrayList;
import java.util.Arrays;

public class ChocolateDistribution {
    public static void main(String[] args) {
        // a: 3 4 1 9 56 7 9 12  -> cada nr é um saco com qnt de chocs lá dentro
        // m: 5                  -> nr de alunos
        // result: 6 (3, 4, 7, 9, 9) (9-3 = 6)
        int distributionNumber = chocolateDistribution(new int[]{3, 4, 1, 9, 56, 7, 9, 12}, 5);
        System.out.println(distributionNumber);
    }

    private static int chocolateDistribution(int[] chocs, int students) {
        Arrays.sort(chocs); // [1, 3, 4, 7, 9, 9, 12, 56]

        int smallest = Integer.MAX_VALUE;
        for (int i = 0, j = students - 1; j < chocs.length; i++, j++) {
            int difference = chocs[j] - chocs[i];
            if(difference < smallest) {
                smallest = difference;
            }
        }
        return smallest;
    }
}
