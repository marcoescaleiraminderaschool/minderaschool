package com.minderaschool.reverseArray;

import java.util.Scanner;

public class ReverseArray {

    public static void main(String[] main) {
        int[] numbers = getNumbers();
        numbers = reverseArray(numbers);

        printNumbers(numbers);
    }

    public static int[] getNumbers() {
        Scanner input = new Scanner(System.in);

        int qty = input.nextInt();
        int[] numbers = new int[qty];

        for (int a = 0; a < qty; a++) {
            numbers[a] = input.nextInt();
        }
        return numbers;
    }

    public static int[] reverseArray(int[] nums) {
        int length = nums.length;
        for (int i = 0; i < length / 2; i++) {
            int temp           = nums[i];
            nums[i]            = nums[length-(1+i)];
            nums[length-(1+i)] = temp;
        }
        return nums;
    }

    public static void printNumbers(int[] nums) {
        for (int num : nums) {
            System.out.print(num + " ");
        }
    }
}
