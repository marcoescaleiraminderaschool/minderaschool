package com.minderaschool.doubleLinkedList;

public class DoublyLinkedList {
    Node start;
    Node end;
    private int size;

    public DoublyLinkedList() {
        this.start = null;
        this.end = null;
        this.size = 0;
    }

    public boolean isEmpty() {
        return start == null;
    }

    public int size() {
        return size;
    }



    public void addAtStart(int val) {
        Node node = new Node(val, null, null);
        if (start == null) {
            start = node;
            end = start;
        } else {
            start.setLinkPrev(node);
            node.setLinkNext(start);
            start = node;
        }
        size++;
    }

    public void addAtPos(int val , int pos) {
        Node nptr = new Node(val, null, null);
        if (pos == 1) {
            addAtStart(val);
            return;
        }
        Node ptr = start;
        for (int i = 2; i <= size; i++) {
            if (i == pos) {
                Node tmp = ptr.getNext();
                ptr.setLinkNext(nptr);
                nptr.setLinkPrev(ptr);
                nptr.setLinkNext(tmp);
                tmp.setLinkPrev(nptr);
            }
            ptr = ptr.getNext();
        }
        size++ ;
    }

    public void addAtEnd(int val) {
        Node node = new Node(val, null, null);
        if (start == null) {
            start = node;
            end = start;
        } else {
            node.setLinkPrev(end);
            end.setLinkNext(node);
            end = node;
        }
        size++;
    }


    public Node get(int index) {
        Node temporaryNode = start;

        int i = 0;
        while (temporaryNode != null && i < index) {
            temporaryNode = temporaryNode.getNext();
            i++;
        }
        return temporaryNode;
    }


    public int getElement(int index) {
        return get(index).element;
    }


    public void add(int val) {
        Node nptr = new Node(val, null, null);
        Node tmp, ptr;
        boolean insert = false;

        if (start == null) {
            start = nptr;
        } else if (val <= start.getElement()) {
            nptr.setLinkNext(start);
            start.setLinkPrev(nptr);
            start = nptr;
        } else {
            tmp = start;
            ptr = start.getNext();
            while(ptr != null) {
                if(val >= tmp.getElement() && val <= ptr.getElement()) {
                    tmp.setLinkNext(nptr);
                    nptr.setLinkPrev(tmp);
                    nptr.setLinkNext(ptr);
                    ptr.setLinkPrev(nptr);
                    insert = true;
                    break;
                } else {
                    tmp = ptr;
                    ptr = ptr.getNext();
                }
            }
            if(!insert) {
                tmp.setLinkNext(nptr);
                nptr.setLinkPrev(tmp);
            }
        }
        size++;
    }


    public void remove(int pos) {
        if (pos == 1) {
            if (size == 1) {
                start = null;
                end = null;
                size = 0;
                return;
            }
            start = start.getNext();
            start.setLinkPrev(null);
            size--;
            return;
        }
        if (pos == size) {
            end = end.getPrev();
            end.setLinkNext(null);
            size--;
        }
        Node ptr = start.getNext();
        for (int i = 2; i <= size; i++) {
            if (i == pos) {
                Node p = ptr.getPrev();
                Node n = ptr.getNext();

                p.setLinkNext(n);
                n.setLinkPrev(p);
                size--;
                return;
            }
            ptr = ptr.getNext();
        }
    }


    public void print() {
        System.out.print("\nDoubly Linked List = ");
        if (size == 0) {
            System.out.print("Empty\n");
            return;
        }
        if (start.getNext() == null) {
            System.out.println(start.getElement());
            return;
        }
        Node ptr = start;
        System.out.print(start.getElement() + " <-> ");
        ptr = start.getNext();
        while (ptr.getNext() != null) {
            System.out.print(ptr.getElement() + " <-> ");
            ptr = ptr.getNext();
        }
        System.out.print(ptr.getElement() + "\n");
        System.out.println();
    }

    public void printAll() {
        Node temporaryNode = start;

        int i = 0;
        do {
            if (i == 0) {
                System.out.println(temporaryNode + " [element:" + temporaryNode.getElement() + ", prev:" + temporaryNode.getPrev() + ", next:" + temporaryNode.getNext() + "]");
            } else if (i == size - 1) {
                System.out.println(temporaryNode + " [element:" + temporaryNode.getElement() + ", prev:" + temporaryNode.getPrev() + ", next:" + temporaryNode.getNext() + "]");
            } else {
                System.out.println(temporaryNode + " [element:" + temporaryNode.getElement() + ", prev:" + temporaryNode.getPrev() + ", next:" + temporaryNode.getNext() + "]");
            }
            temporaryNode = temporaryNode.getNext();

            i++;
        } while (temporaryNode != null && i < size);
    }
}
