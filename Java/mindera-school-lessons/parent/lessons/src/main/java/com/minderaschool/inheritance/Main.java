package com.minderaschool.inheritance;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Figure> list = new ArrayList<Figure>();
        list.add(new Circle(8));
        list.add(new Triangle(33, 11));
        list.add(new Rectangle(14, 5));
        list.add(new Square(7));

        for (Figure fig : list) {
            fig.whoAmI();
            System.out.printf("Area: %.2f \n\n", fig.areaValue);
        }

    }
}

