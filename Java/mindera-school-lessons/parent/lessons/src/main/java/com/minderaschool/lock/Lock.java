package com.minderaschool.lock;

public class Lock {
    static int lock(int[] code, int[] key) {
        return
                and(
                        and(
                                xnor(code[0], key[0]),
                                xnor(code[1], key[1])
                        ),
                        and (
                                xnor(code[2], key[2]),
                                xnor(code[3], key[3])
                        )
                );
    }

    static int xnor(int i1, int i2) {
        return not(xor(i1, i2));
    }

    static int xor(int i1, int i2) {
        return and( or(i1, i2), not(and(i1, i2)) );
    }

    static int not(int i1) {
        return and(0x01, ~i1);
    }

    static int and(int i1, int i2) {
        return i1 & i2;
    }

    static int or(int i1, int i2) {
        return i1 | i2;
    }
}
