package com.minderaschool.myLinkedList;

public class Node {
    private int element;
    Node next;

    public Node() {
        this.element = 0;
        this.next = null;
    }

    public Node(int element, Node next) {
        this.element = element;
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public int getElement() {
        return element;
    }

    public void setElement(int element) {
        this.element = element;
    }
}
