package com.minderaschool.maps;

import java.util.HashMap;
import java.util.Map;

public class Hashmap2 {
    public static void main(String[] args) {
        //Ter uma hashmap de integer, char e remover todos as keys exceto a ultima com o mesmo char
        HashMap<Integer, Character> mapChar = new HashMap<>() {{
            put(1, 'a');
            put(2, 'b');
            put(3, 'c');
            put(4, 'd');
            put(5, 'a');
            put(6, 'a');
        }};

        HashMap<Integer, Character> mapCharPrint = new HashMap<>();

        for (Map.Entry<Integer, Character> entry : mapChar.entrySet()) {
            Integer key = null;
            if (mapCharPrint.containsValue(entry.getValue())) {
                for (Map.Entry entry1 : mapCharPrint.entrySet()) {
                    if (entry1.getValue().equals(entry.getValue())) {
                        key = (Integer) entry1.getKey();
                        //System.out.mapCharPrintln("KEY: " + key);
                        break;
                    }
                }
                mapCharPrint.remove(key);
            }
            mapCharPrint.put(entry.getKey(), entry.getValue());

        }
        mapCharPrint.forEach((k, v) -> System.out.println(k + " - " + v));
    }
}
