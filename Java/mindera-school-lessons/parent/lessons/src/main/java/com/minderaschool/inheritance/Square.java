package com.minderaschool.inheritance;

public class Square extends Rectangle {

    public Square(double side) {
        super.whoAmI = "Square";
        super.areaValue = side * side;
    }

    public double area() {
        return super.areaValue;
    }

    public void whoAmI() {
        System.out.println(super.whoAmI);
    }
}
