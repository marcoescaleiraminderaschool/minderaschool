package com.minderaschool.tree;

public class Node {
    private Node left, right;
    private int value;

    public Node(int val) {
        left = right = null;
        value = val;
    }

    public Node getLeft() {
        return left;
    }
    public void setLeft(Node left) {
        this.left = left;
    }


    public Node getRight() {
        return right;
    }
    public void setRight(Node right) {
        this.right = right;
    }


    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }
}
