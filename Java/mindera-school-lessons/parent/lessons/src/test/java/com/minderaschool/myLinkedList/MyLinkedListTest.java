package com.minderaschool.myLinkedList;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

// todo: bug de .equals() with Lists of different sizes
//       .validateIndex(0) that checks bounds of List

class MyLinkedListTest {
    private MyLinkedList list;
    private MyLinkedList testList;

    @BeforeEach
    void setUp() {
        list = new MyLinkedList();
    }

    @AfterEach
    void tearDown() {
        list = null;
    }


    // isEmpty() || 2 test cases
    @Test
    void isEmpty() {
        assert (list.isEmpty());
    }

    @Test
    void isNotEmpty() {
        list.addAtHead(1);
        assert (!list.isEmpty());
    }


    /*
     *  addAtHead() || 2 test cases
     */
    @Test
    void addAtHead_EmptyListHead() {
        assert (list.isEmpty());

        list.addAtHead(1);
        assert (list.get(0) == 1);
    }

    @Test
    void addAtHead() {
        list.addAtHead(2);
        assert (!list.isEmpty());

        list.addAtHead(1);
        assert (list.get(0) == 1);
    }


    /*
     *  addAtTail() || 2 test cases
     */
    @Test
    void addAtTail_EmptyList() {
        assert (list.isEmpty());

        list.addAtTail(1);
        assert (list.get(0) == 1);
    }

    @Test
    void addAtTail() {
        list.addAtTail(1);
        assert (!list.isEmpty());

        list.addAtTail(2);
        assert (list.get(1) == 2);
    }


    /*
     *  addAtIndex() || 3 test cases
     */
    @Test
    void addAtIndex_EmptyList() {
        assert (list.isEmpty());

        list.addAtIndex(0, 1);
        assert (list.get(0) == 1);
    }

    @Test
    void addAtIndex_ListSize() {
        list.addAtIndex(0, 1);
        list.addAtIndex(1, 2);
        assert(!list.isEmpty());

        list.addAtIndex(list.size(), 3);
        assert (list.get(list.size() - 1) == 3);
    }

    @Test
    void addAtIndex_Middle() {
        list.addAtIndex(0, 1);
        list.addAtIndex(1, 3);
        assert (!list.isEmpty());

        list.addAtIndex(1, 2);
        assert (list.get(1) == 2);
    }


    /*
    * get() || 1 test cases
    */
    @Test
    void get() {
        list.addAtIndex(0, 1);
        assert (list.get(0) == 1);
    }


    /*
    *  equals() || 2 test cases
    */
    @Test
    void equals_DifferentSize() {
        list.addAtIndex(0, 1);
        list.addAtIndex(1, 2);
        list.addAtIndex(2, 3);
        assert(!list.isEmpty());
        System.out.println("list: " + list.size());
        // BUG HERE
        testList = new MyLinkedList();
        System.out.println("newList: " + list.size());
        assert(!list.equals(new MyLinkedList()));
    }

    @Test
    void equals() {
        list.addAtIndex(0, 1);
        assert(!list.isEmpty());

        MyLinkedList ll = new MyLinkedList() {{ addAtIndex(0, 2); }};
        assert(!list.equals(ll));
    }


    /*
    * deleteAtIndex() || 2 test cases
    */
    @Test
    void deleteAtIndex_FirstElement() {
        list.addAtIndex(0, 1);
        list.addAtIndex(1, 2);
        assert(!list.isEmpty());

        list.deleteAtIndex(0);
        assert (list.get(0) == 2);
    }

    @Test
    void deleteAtIndex() {
        list.addAtIndex(0, 1);
        list.addAtIndex(1, 2);
        assert(!list.isEmpty());

        list.deleteAtIndex(1);
        assert (list.get(0) == 1);
    }

    @Test
    void isPalindrome() {
        list.addAtHead(1);
        list.addAtIndex(1, 2);
        list.addAtTail(1);
        assert(!list.isEmpty());

        assert (list.isPalindrome());
    }

    @Test
    void isNotPalindrome() {
        list.addAtHead(1);
        list.addAtIndex(1, 2);
        list.addAtTail(2);
        assert(!list.isEmpty());

        assert (!list.isPalindrome());
    }
}