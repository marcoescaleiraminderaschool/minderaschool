package com.minderaschool.utils;

import static org.junit.jupiter.api.Assertions.*;

class ConverterTest {

    @org.junit.jupiter.api.Test
    void romanToDecimalFor1() {
        assertEquals(1, Converter.romanToDecimal("I"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalFor15() {
        assertEquals(15, Converter.romanToDecimal("XV"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalFor1556() {
        assertEquals(1556, Converter.romanToDecimal("MDLVI"));
    }

    @org.junit.jupiter.api.Test
    void romanToDecimalFor4() {
        assertEquals(1554, Converter.romanToDecimal("MDLIV"));
    }
}