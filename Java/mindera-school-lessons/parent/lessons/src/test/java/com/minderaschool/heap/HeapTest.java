package com.minderaschool.heap;

import org.junit.jupiter.api.Test;

class HeapTest {

    @Test
    void isMinHeap() {
        assert (Heap.isMinHeap(new int[]{0,3,4,5,10,7,15}));
    }

    @Test
    void isNotMinHeap() {
        assert (!Heap.isMinHeap(new int[]{4,3,4,5,10,7,15}));
    }

    @Test
    void isMaxHeap() {
        assert (Heap.isMaxHeap(new int[]{10, 9, 5, 6, 3, 2}));
    }

    @Test
    void isNotMaxHeap() {
        assert (!Heap.isMaxHeap(new int[]{1, 9, 5, 6, 3, 2}));
    }
}