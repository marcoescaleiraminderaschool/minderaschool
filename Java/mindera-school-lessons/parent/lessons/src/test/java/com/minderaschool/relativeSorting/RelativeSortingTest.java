package com.minderaschool.relativeSorting;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.minderaschool.relativeSorting.RelativeSorting.relativeSorting2;


class RelativeSortingTest {
    @Test
    void relativeSorting() {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(8);
        list.add(9);
        list.add(2);
        list.add(3);
        list.add(7);
        list.add(5);
        list.add(13);
        list.add(9);
        list.add(2);
        list.add(8);
        ArrayList<Integer> sortOrder = new ArrayList<>();
        sortOrder.add(9);
        sortOrder.add(3);
        sortOrder.add(13);
        sortOrder.add(5);

        List<Integer> a = relativeSorting2(list, sortOrder);

        for (Integer i : a) {
            System.out.print(i + " - ");
        }

        // 8 9 2 3 7 5 13 9 2 8
        // 9 9 3 13 5 2 2 7 8 8

        // 9 3 13 5
    }
}