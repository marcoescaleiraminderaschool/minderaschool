package com.minderaschool.tree;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class TreeTest {
    Tree bt;

    @BeforeEach
    void setUp() {
        bt = new Tree();
    }

    @Test
    void ordersTest() {
        bt.insertStart(1);
        Node root = bt.getRoot();

        Node firstL = bt.insertLeft(root, 2);
        bt.insertLeft(firstL, 3);
        bt.insertRight(firstL, 4);

        Node firstR = bt.insertRight(root, 5);
        bt.insertLeft(firstR, 6);
        bt.insertRight(firstR, 7);

        System.out.print(" Pre-order: ");
        bt.preOrder(root);

        System.out.println();

        System.out.print("  In-order: ");
        bt.inOrder(root);

        System.out.println();

        System.out.print("Post-order: ");
        bt.postOrder(root);

        assert (true);
    }

}