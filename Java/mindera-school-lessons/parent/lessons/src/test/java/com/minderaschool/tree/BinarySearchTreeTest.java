package com.minderaschool.tree;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTreeTest {
    BinarySearchTree bst;
    @BeforeEach
    void setUp() {
        bst = new BinarySearchTree();
    }

    @Test
    void insertNode() {
        Node startNode = bst.insertNode(50);
        bst.insertNode(startNode, 21);
        bst.insertNode(startNode, 4);
        bst.insertNode(startNode, 32);
        bst.insertNode(startNode, 76);
        bst.insertNode(startNode, 64);
        bst.insertNode(startNode, 100);
        bst.insertNode(startNode, 52);
        /*
                            50
                         /     \
                       21       76
                     /   \     /  \
                    4    32   64  100
                             /
                            52
         */
        assertTrue (bst.findNode(startNode, 52)); // 50 76 64 52
        System.out.println("\n------------");
        assertTrue (bst.findNode(startNode, 32)); // 50 21 32
    }
}