package com.minderaschool.fibonacci;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

class FibonacciTest {

    @Test
    void fibonacci_1() {
        long[] arr = new long[]{1};
        long[] fib = Fibonacci.fibSeq(1);
        System.out.println(Arrays.toString(fib));

        assert (Arrays.equals(arr, fib));
    }

    @Test
    void fibonacci_2() {
        long[] arr = new long[]{1,1};
        long[] fib = Fibonacci.fibSeq(2);
        System.out.println(Arrays.toString(fib));

        assert (Arrays.equals(arr, fib));
    }

    @Test
    void fibonacci_3() {
        long[] arr = new long[]{1,1,2};
        long[] fib = Fibonacci.fibSeq(3);
        System.out.println(Arrays.toString(fib));

        assert (Arrays.equals(arr, fib));
    }

    @Test
    void fibonacci_4() {
        long[] arr = new long[]{1,1,2,3};
        long[] fib = Fibonacci.fibSeq(4);
        System.out.println(Arrays.toString(fib));

        assert (Arrays.equals(arr, fib));
    }

    @Test
    void fibonacci_7() {
        long[] arr = new long[]{1,1,2,3,5,8,13};
        long[] fib = Fibonacci.fibSeq(7);
        System.out.println(Arrays.toString(fib));

        assert (Arrays.equals(arr, fib));
    }
}