package com.minderaschool.sorter;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class SortTest {
    @Test
    void canHandleEmptyArray() {
        Sort sut = new Sort();

        int[] toSortArray = new int[0];
        int[] expectedSortedArray = new int[0];

        int[] sortedArray = sut.integerArray(toSortArray);

        assertArrayEquals(expectedSortedArray, sortedArray);
    }

    @Test
    void canHandleArrayWithOneElement() {
        Sort sut = new Sort();

        int[] toSortArray = new int[]{1};
        int[] expectedSortedArray = new int[]{1};

        int[] sortedArray = sut.integerArray(toSortArray);

        assertArrayEquals(expectedSortedArray, sortedArray);
    }

    @Test
    void canHandleArrayWithTwoElements() {
        Sort sut = new Sort();

        int[] toSortArray = new int[]{2,1};
        int[] expectedSortedArray = new int[]{1,2};

        int[] sortedArray = sut.integerArray(toSortArray);

        assertArrayEquals(expectedSortedArray, sortedArray);
    }

    @Test
    void canHandleArrayWithTwoElementsWithoutSwapping() {
        Sort sut = new Sort();

        int[] toSortArray = new int[]{1,2};
        int[] expectedSortedArray = new int[]{1,2};

        int[] sortedArray = sut.integerArray(toSortArray);

        assertArrayEquals(expectedSortedArray, sortedArray);
    }

    @Test
    void defaultOrderBubbleSortIntegerArray() {
        Sort sut = new Sort();

        int[] toSortArray = new int[]{6, 2, 7, 1, 5, 10};
        int[] expectedSortedArray = new int[]{1, 2, 5, 6, 7, 10};

        int[] sortedArray = sut.integerArray(toSortArray);

        assertArrayEquals(expectedSortedArray, sortedArray);
    }

    @Test
    void ascOrderBubbleSortIntegerArray() {
        Sort sut = new Sort();

        int[] toSortArray = new int[]{6, 2, 7, 1, 5, 10};
        int[] expectedSortedArray = new int[]{1, 2, 5, 6, 7, 10};

        int[] sortedArray = sut.integerArray(toSortArray, SortOrder.ASCENDING);

        assertArrayEquals(expectedSortedArray, sortedArray);
    }

    @Test
    void descOrderBubbleSortIntegerArray() {
        Sort sut = new Sort();

        int[] toSortArray = new int[]{6, 2, 7, 1, 5, 10};
        int[] expectedSortedArray = new int[]{10, 7, 6, 5, 2, 1};

        int[] sortedArray = sut.integerArray(toSortArray, SortOrder.ASCENDING);

        assertArrayEquals(expectedSortedArray, sortedArray);
    }

    @Test
    void bubbleSortNegativeWithNumbers() {
        Sort sut = new Sort();

        int[] toSortArray = new int[]{6, 2, 7, -1, 5, 10, 0};
        int[] expectedSortedArray = new int[]{-1, 0, 2, 5, 6, 7, 10};

        int[] sortedArray = sut.integerArray(toSortArray);

        assertArrayEquals(expectedSortedArray, sortedArray);
    }
}