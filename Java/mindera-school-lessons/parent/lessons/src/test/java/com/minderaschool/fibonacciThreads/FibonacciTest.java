package com.minderaschool.fibonacciThreads;

import org.junit.jupiter.api.Test;

class FibonacciTest {
    @Test
    void threadsWithFibonacci() throws InterruptedException {
        Fibonacci sut = new Fibonacci();

        int times = 100;
        long sum = 0;
        long min = Long.MAX_VALUE;
        long max = Long.MIN_VALUE;
        for (int i = 0; i < times; i++) {
            long n = (long) i;
            long start = System.nanoTime();
            long fib = sut.fibonacci(n);
            long finish = System.nanoTime()-start;
            sum += finish;
            if (finish > max) {
                max = finish;
            }
            if (finish < min) {
                min = finish;
            }
            System.out.printf("\n%4d - %d ns", fib, finish);
        }
        System.out.printf("\n\nMin: %d ns", min);
        System.out.printf("\nAvg: %d ns", (sum/times));
        System.out.printf("\nMax: %d ns\n", max);
    }

    @Test
    void name() throws InterruptedException {
        Fibonacci sut = new Fibonacci();
        long start = System.nanoTime();
        long fib = sut.fibonacci(10000L);
        long finish = System.nanoTime() - start;

        System.out.printf("\n%4d - %d ns", fib, finish);
    }
}