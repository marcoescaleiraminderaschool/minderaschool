import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

// TCP - Client

class Client {
    public static void main(String[] args) throws IOException {
        //Socket s = new Socket("localhost", 4999);
	    Socket s = new Socket("towel.blinkenlights.nl", 80);
        
        PrintWriter pr = new PrintWriter(s.getOutputStream());
        pr.println("hello");
        pr.flush();
        
        InputStreamReader in = new InputStreamReader(s.getInputStream());
        BufferedReader bf = new BufferedReader(in);
        String str = null;

        while(s.isConnected() && (str = bf.readLine()) != null) {
            System.out.println(str);
        }

        s.close();
    }
}
