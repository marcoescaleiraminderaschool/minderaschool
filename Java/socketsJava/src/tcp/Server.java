import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

// TCP - Server

class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket ss = new ServerSocket(4999);
        Socket s = ss.accept();

        while(s.isConnected()) {
            InputStreamReader in = new InputStreamReader(s.getInputStream());
            BufferedReader bf = new BufferedReader(in);

            String str = bf.readLine();
            System.out.println("Client: " + str);

            PrintWriter pr = new PrintWriter(s.getOutputStream());
            pr.println(str.toUpperCase());
            pr.flush();
        }

        ss.close();
    }
}
