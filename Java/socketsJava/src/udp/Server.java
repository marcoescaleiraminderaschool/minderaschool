import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;

// UDP - Server

public class Server {
    public static void main(String[] args) throws IOException {
        DatagramSocket ds = new DatagramSocket(5000);

        byte[] receivedData = new byte[1024];

        while (true) {
            DatagramPacket receivedPacket = new DatagramPacket(receivedData, receivedData.length);
            ds.receive(receivedPacket);

            String str = new String(receivedPacket.getData());
            System.out.println("Client: " + str.toUpperCase());
        }
    }
}
