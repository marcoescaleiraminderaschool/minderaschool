import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

// UDP - Client

public class Client {
    public static void main(String[] args) throws IOException {
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress iPAddress = InetAddress.getByName("localhost");
        
        String sentence = "hello";
        byte[] sendData = sentence.getBytes();
        
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, iPAddress, 5000);
        
        clientSocket.send(sendPacket);
        clientSocket.close();
    }
}
