import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the plusMinus function below.
    static void plusMinus(int[] arr) {
        //Verificar quantos numeros positivos, negativos ou zeros existem
        int length = arr.length;
        int positive = 0;
        int negative = 0;
        int zero     = 0;

        for (int i = 0; i < length; i++) {
            if (arr[i] > 0) { //Positivo
                positive ++;
            }
            if (arr[i] == 0) { //Zero
                zero ++;
            }
            if (arr[i] < 0) { //Negativo
                negative ++;
            }
        }

        float positivesPorpotion = (float)positive / length;
        float zerosPorpotion = (float)zero / length;
        float negativesPorpotion = (float)negative / length;

        
        System.out.printf("%.6f\n%.6f\n%.6f", positivesPorpotion, negativesPorpotion, zerosPorpotion);
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        plusMinus(arr);

        scanner.close();
    }
}
