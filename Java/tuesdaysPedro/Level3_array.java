//Write a Java program to find the maximum and minimum value of an array.
//Write a Java program to reverse an array of integer values.

public class Level3_array {

    public static void main (String[] args) {
        int[] values = {1, 2, 3, 4, 5, 6, 7};

        findMaxMin(values);

        reverseArray(values);
    }


    static void reverseArray(int[] array) {
        for(int i = 0; i < array.length / 2; i++) {
            int temp                    = array[i];
            array[i]                    = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }

        System.out.print("\nReversed array: ");
        for (int num: array)
            System.out.printf("%d ", num);
    }


    static void findMaxMin(int[] array) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;

        for (int num : array) {
            if (num > max)
                max = num;
            if (num < min)
                min = num;
        }

        System.out.printf("\nMin: %d || Max: %d \n", min, max);
    }
}
