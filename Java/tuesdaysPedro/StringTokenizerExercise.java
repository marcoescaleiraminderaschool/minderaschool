import java.util.StringTokenizer;

public class StringTokenizerExercise {
    public static void main(String[] args) {
        StringTokenizer st = new StringTokenizer("A Maria compra uma camisa amarela");

        while (st.hasMoreElements()) {
            String wordString = st.nextToken();
            char[] wordChar = wordString.toCharArray();
            int a = 0;
            for (int i = 0; i < wordChar.length; i++) {
                if (wordChar[i] == 'a' || wordChar[i] == 'A') {
                    a++;
                }
            }
            if(a > 1) {
                System.out.print(" " + wordString);
            }
        }

    }
}
