import java.util.*;

// Check if user doesnt insert > 6
// Save game status

public class Translacao {
    public static int QTY_COORDS = 4; //Equals to 2 coords (Ex: 0, 4 & 1, 2)

    public static void main(String[] args) {
        // int[] size = getGridSize();
        int[] size = new int[2];
        Scanner userInput = new Scanner(System.in);

        System.out.println("First insert the size of the grid \n");

        System.out.print("Height: ");
        size[0] = userInput.nextInt();

        System.out.print("\nLength: ");
        size[1] = userInput.nextInt();

        int[][] grid = new int[size[0]][size[1]];
        int[] coords = new int[QTY_COORDS];

        displayGrid(grid);    // Start by showing the grid empty

        coords = getCoords(size); //get user coordinates

        // System.out.println(Arrays.toString(coords));

        makeDiagonal(grid, coords);

        makeTranslation(grid, coords);

        displayGrid(grid);
    }

    public static void makeTranslation(int[][] grid, int[] coords) {
        int[] coordinates = manageCoords(coords);

        int startY  = coordinates[0] - 1;
        int startX  = coordinates[1] + 1;
        int endY    = coordinates[2] - 1;
        int endX    = coordinates[3] + 1;

        int[] arr = {startY, startX, endY, endX};

        if ((startX < 0 || startX >= grid.length) || (endX<=0 || endX>=grid.length) ||
                (startY<0 || startY>=grid.length) || (endY<=0 || endY>=grid.length)) {
            System.out.println("\nWARNING: Translation is out of bounds");
        } else {
            //make the translation
            makeDiagonal(grid, arr);
        }
    }

    public static int[] manageCoords(int[] coords) {
        // Find the lowest cord
        int startY  = 0;
        int startX  = 0;
        int endY    = 0;
        int endX    = 0;
        boolean impossible = false;

        if(coords[1] < coords[3]){
            startY = coords[1]; startX = coords[0];
            endY   = coords[3]; endX = coords[2];
        } else if(coords[1] > coords[3]) {
            startY = coords[3]; startX = coords[2];
            endY   = coords[1]; endX = coords[0];
        }
        int[] array = {startY, startX, endY, endX};
        return array;
    }

    public static void makeDiagonal(int[][] grid, int[] coords) {
        int[] coordinates = manageCoords(coords);
        int startY  = coordinates[0];
        int startX  = coordinates[1];
        int endY    = coordinates[2];
        int endX    = coordinates[3];

        //Check if it's a diagonal
        if(Math.abs(startX - endX) == Math.abs(startY - endY)) {
            // Create start points
            grid[startY][startX] = 1; //Start coord
            grid[endY][endX]     = 2; //End coord

            //Check type of diagonal
            if(startX > endX)
                minorDiagonal(grid, startY, startX, endY, endX);
            else
                majorDiagonal(grid, startY, startX, endY, endX);

        } else {
            System.out.println("\nThe coordinates you inserted are not a proper diagonal");
        }

        return;
    }

    public static void majorDiagonal(int[][] grid, int startY, int startX, int endY, int endX) {
        for (int y = startY + 1, x = startX + 1; y < endY && x < endX; y++, x++) //Fill the major diagonal
            grid[y][x] = 5;
    }

    public static void minorDiagonal(int[][] grid, int startY, int startX, int endY, int endX) {
        for (int y = startY + 1, x = startX - 1; y < endY && x >= endX; y++, x--)
            grid[y][x] = 5;
    }

    public static int[] getCoords (int[] size) {
        Scanner scanner = new Scanner(System.in);
        int[] coords = new int[4];
        int Y = size[0], X = size[1];
        int y = 0, x = 0;

        System.out.println("\nFirst coordinate");
        do {
            System.out.print("x: ");
            x = scanner.nextInt();

            System.out.print("y: ");
            y = scanner.nextInt();

            if(x >= X || y >= Y)
                System.out.println("Please insert coordinates x( 0 - " + (X-1) + " ) and y( 0 - " + (Y-1) + " )");
        } while (x >= X || y >= Y);

        coords[0] = x;
        coords[1] = y;

        System.out.println("\nSecond coordinate");
        do {
            System.out.print("x: ");
            x = scanner.nextInt();

            System.out.print("y: ");
            y = scanner.nextInt();

            if(x >= X || y >= Y)
                System.out.println("Please insert coordinates x( 1 - " + (X-1) +" ) and y( 1 - " + (Y-1) + " )");
        } while (x >= X || y >= Y);

        coords[2] = x;
        coords[3] = y;

        System.out.println();

        scanner.close();
        return coords;
    }

    public static void displayGrid(int[][] grid) {
        for (int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid[i].length; j++) {

                System.out.printf(" | %d", grid[i][j]);
            }

            System.out.print(" |");
            System.out.println();
        }
    }

}
