import java.util.Arrays;
import java.util.StringTokenizer;

public class StringTokenizerExercise2 {
    public static void main(String[] args) {
        StringTokenizer st = new StringTokenizer("0 9 9 2 3 7 0 1 0 6");
        int sum = 0;
        int counter = 10;

        while (st.hasMoreElements()) {
            sum += counter * Integer.parseInt(st.nextToken());
            counter --;
        }

        if(sum % 11 == 0) {
            System.out.println("Está correto");
        } else {
            System.out.println("Está errado");
        }

    }
}
