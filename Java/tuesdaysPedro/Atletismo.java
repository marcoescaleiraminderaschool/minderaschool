import java.util.*;
// import Utils.*;

public class Atletismo {
    public static void main(String[] args) {
        int preco = 1000;
        float larguraPista = 6.4f;
        int larguraFutebol = 60;
        int metadeLarguraFutebol = larguraFutebol / 2;
        int comprimentoFutebol = 80;
        float raioCirculos = metadeLarguraFutebol + larguraPista;
        float pi = 3.14f;

        //Calcular a área do cirulo interior, que está dividido
        float AreaCirculoInterior = pi * (metadeLarguraFutebol * metadeLarguraFutebol);

        //Calcular a área do circulo exterior e interior junto
        float AreaCirculoExteriorInterior = pi * (raioCirculos * raioCirculos);
        
        float areaPista = (AreaCirculoExteriorInterior - AreaCirculoInterior) + ((comprimentoFutebol * larguraPista) * 2);

        float precoAreaPista = areaPista * preco;

        // Prints.printFloat(precoAreaPista);
        System.out.println("Custo da pista: " + precoAreaPista + " €");
    }
}