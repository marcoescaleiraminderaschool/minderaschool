import java.util.*;
import Utils.*;

public class Multibanco {
    public static int[] billsAmount = new int[4];
    public static int amount = 0;

    public static void main(String[] args) {
        int[] bills = {5, 10, 20, 50};

        Scanner scanner = new Scanner(System.in);

        Prints.printString("How much money?");
        int money = scanner.nextInt();

        if(money % 5 == 0) {
            billsAmount[3] = check(50);
            billsAmount[2] = check(20);
            billsAmount[1] = check(10);
            billsAmount[0] = check(5);

            for (int i = 0; i < billsAmount.length; i++) {
                System.out.println(billsAmount[i] + " nota/s de " + bills[i]);
            }
        } else {
            Prints.printString("Insert a number multiple of 5");
        }

        scanner.close();
    }

    public static int check(int bill) {
        int n = 0;
        amount -= bill;
        while (amount > 0 && amount >= bill) {
            amount -= bill;

            n++;

            if(amount == 0) {
                return n;
            }
            if (amount < 0) {
                n--;
                amount += bill;
                return n;
            }
        }
        return n;
    }
}