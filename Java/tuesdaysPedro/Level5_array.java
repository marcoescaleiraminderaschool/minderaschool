// Write a Java program to add two matrices of the same size

public class Level5_array {
    public static void main(String[] args) {
        int[][] first  = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] second = {{3, 2, 1}, {6, 5, 4}, {9, 8, 7}};

        sumMatrices(first, second);
    }

    public static void sumMatrices(int[][] first, int[][] second) {
        for (int y = 0; y < first.length; y++)
            for (int x = 0; x < first[y].length; x++)
                System.out.printf("%d + %d = %d\n", first[y][x], second[y][x], (first[y][x] + second[y][x]));
            
    }//sumMatrices

}//Class
