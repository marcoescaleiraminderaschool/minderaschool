import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class PhraseEncript {
    public static void main(String[] args) {
        StringTokenizer phrase = new StringTokenizer("este exame vai correr bem");
        char[] alpha_key = {'e', 'r', 'x'};
        int inc = 2;

        while (phrase.hasMoreTokens()) {
            String wordString = phrase.nextToken();
            char[] wordChar = wordString.toCharArray();

            for (int i = 0; i < wordChar.length; i++) {
                boolean checkEqual = false;
                for (int a = 0; a < alpha_key.length; a++) {
                    if(alpha_key[a] == wordChar[i]) {
                        checkEqual = true;
                    }
                }
                if (!checkEqual) {
                    wordChar[i] += inc;
                    inc++;
                }
            }
            System.out.println(" " + Arrays.toString(wordChar));
        }


    }
}
