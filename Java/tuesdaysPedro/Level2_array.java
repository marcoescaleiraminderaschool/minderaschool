import java.util.Scanner;

public class Level2_array {
    public static void main (String[] args) {
        String[] skiResort = {
                "Whistler Blackcomb", "Squaw Valley", "Brighton",
                "Snowmass", "Sun Valley", "Taos"
        };

        printString(skiResort);

        int[] intArray = {2, 3, 5, 7, 8};

        int sum = printSum(intArray);

        avg(sum, intArray.length);

        checkValue(intArray);

        checkIndex(intArray);
    }


    static void checkIndex(int[] array) {
        Scanner scanner = new Scanner(System.in);
        Integer index = null;

        System.out.print("\nValue: ");
        int value = scanner.nextInt();

        for (int i = 0; i < array.length; i++)
            if (value == array[i]) {
                index = i;
                break;
            }


        if (index != null)
            System.out.println("Index " + index);
        else
            System.out.println("Value not found");
    }


    static void checkValue(int[] array) {
        Scanner scanner = new Scanner(System.in);
        boolean check = false;

        System.out.print("\nValue: ");
        int value = scanner.nextInt();

        for (int i = 0; i < array.length; i++)
            if (value == array[i])
                check = true;

        if (check)
            System.out.println("Correct");
        else
            System.out.println("Value not found");
    }


    static void avg (int sum, int length) {
        int avg = sum / length;
        System.out.println("\nAvg: " + avg);
    }


    static int printSum(int[] array) {
        int sum = 0;

        for (int i = 0; i < array.length; i++)
            sum += array[i];

        System.out.println("\nSum: " + sum);

        return sum;
    }


    static void printString(String[] string) {
        for (String word : string)
            System.out.println(word);
    }
}
