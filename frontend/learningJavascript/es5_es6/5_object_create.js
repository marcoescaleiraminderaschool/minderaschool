// Object of Protos
const bookProtos = {
    getSummary: function () {
        return `${this.title} was written by ${this.author} in ${this.year}`;
    },
    getAge: function () {
        const years = new Date().getFullYear() - this.year;
        return `${this.title} is ${years} years old`;
    }
};

// const book1 = Object.create(bookProtos);
// book1.title  = 'Book One';
// book1.author = 'John Murphy';
// book1.year   = 2015;


const book2 = Object.create(bookProtos, {
    title:  { value: 'Book One'},
    author: { value: 'John Murphy'},
    year:   { value: 2015},
});

console.log(book2.getSummary());

// END OF ES5 PROTOTYPES