import React, { Component } from 'react';

import '../styles/main.scss';

class App extends Component {
  handleTitleClick() {
    alert("You Clicked the Title!");
  }

  render() {
    return (
      <div>
        <h1 onClick={this.handleTitleClick}>My First Parcel React App!</h1>
      </div>
    );
  }
}

export default App;