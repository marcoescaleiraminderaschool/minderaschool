const Prices = ({ bpi }) => {
  const [currency, setCurrency] = React.useState("EUR");

  const currencySymbol =
    currency === "EUR"
      ? "€"
      : currency === "GBP"
      ? "£"
      : currency === "USD"
      ? "$"
      : "";
  const { description, code, rate } = bpi[currency];
  return (
    <div className="container">
      <div className="d-flex flex-column">
        <select
          className="custom-select mb-3"
          value={currency}
          onChange={e => setCurrency(e.target.value)}
        >
          {Object.keys(bpi).map(coin => (
            <option value={coin} key={coin}>
              {coin}
            </option>
          ))}
        </select>
        <ul className="list-group">
          <li className="list-group-item">
            Bitcoin rate for {description}:{" "}
            <span className="badge badge-primary">{code}</span>{" "}
            <b>
              {rate.split(".")[0]}
              {currencySymbol}
            </b>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Prices;
