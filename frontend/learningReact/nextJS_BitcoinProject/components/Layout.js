import Head from "next/head";
import Navbar from "./Navbar";

const Layout = ({ children }) => (
  <div>
    <Head>
      <title>BitsPrice</title>
      <link
        rel="stylesheet"
        href="https://bootswatch.com/4/lux/bootstrap.min.css"
      />
    </Head>
    <Navbar />
    <main className="container">{children}</main>
  </div>
);

export default Layout;
