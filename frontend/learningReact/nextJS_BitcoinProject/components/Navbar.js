import Link from "next/link";

const Navbar = () => (
  <nav className="navbar navbar-expand navbar-dark bg-dark d-flex mb-4">
    <Link href="/">
      <a className="navbar-brand" href="#">
        BitsPrice
      </a>
    </Link>

    <div className="flex-grow-1 d-flex justify-content-end">
      <ul className="navbar-nav">
        <li className="nav-item">
          <Link href="/">
            <a className="nav-link">Home</a>
          </Link>
        </li>
        <li className="nav-item">
          <Link href="/about">
            <a className="nav-link">About</a>
          </Link>
        </li>
      </ul>
    </div>
  </nav>
);

export default Navbar;
