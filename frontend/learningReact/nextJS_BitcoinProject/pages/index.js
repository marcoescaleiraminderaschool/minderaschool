import Fetch from "isomorphic-unfetch";
import Layout from "../components/Layout";
import Prices from "../components/Prices";

const Index = ({ bpi, date, disclaimer }) => (
  <Layout>
    <h1>Bem vindo à BitsPrice</h1>
    <p>Check current bitcoin rate</p>
    <Prices bpi={bpi} />
  </Layout>
);

Index.getInitialProps = async () => {
  const res = await Fetch("https://api.coindesk.com/v1/bpi/currentprice.json");
  const data = await res.json();

  return {
    bpi: data.bpi,
    date: data.time.updatedISO,
    disclaimer: data.disclaimer
  };
};

export default Index;
