// JSX - JavaScript XML
const app = {
    title: 'Indecision App',
    subtitle: 'This is some info',
    options: []
  };
  
  const onFormSubmit = (e) => {
    e.preventDefault();
  
    const option = e.target.elements.option.value;
    if (option) {
      app.options.push(option);
      e.target.elements.option.value = '';
      render();
    }
  };
  
  const removeAll = () => {
    app.options = [];
    render();
  };
  
  const onMakeDecision = () => {
    const randmNum = Math.floor(Math.random() * app.options.length);
    const option = app.options[randmNum];
    alert(option);
  };
  
  const render = () => {
    const template = (
        <div>
          <h1>{ app.title }</h1>
          { app.subtitle && <p>{ app.subtitle }</p> }
          <p>{app.options.length > 0 ? 'Here are your options' : 'No options'}</p>
          <button disabled={app.options.length === 0} onClick={onMakeDecision}>What should I do</button>
          <button onClick={removeAll}>Remove All</button>
          <ul>
            {
              app.options.map((opt) => <li key={opt}> {opt} </li>)
            }
          </ul>
          <form onSubmit={onFormSubmit}>
            <input type="text" name="option" />
            <button type="submit">Add Option</button>
          </form>
        </div>
    );
    ReactDOM.render(template, document.getElementById('app'));
  };
  
  render();
  
  