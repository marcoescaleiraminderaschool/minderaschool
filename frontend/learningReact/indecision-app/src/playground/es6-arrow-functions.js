const square = function (x) {
    return x * x;
}

const squareArrow = (x) => {
    return x * x;
}

const squareArrow2 = (x) => x * x;

console.log(square(5));
console.log(squareArrow(5));
console.log(squareArrow2(5));

//Challenge
const getFirstName = (name) => name.split(' ')[0];

console.log(getFirstName('Mike Smith'));