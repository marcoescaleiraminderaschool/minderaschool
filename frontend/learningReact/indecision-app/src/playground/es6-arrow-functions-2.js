// arguments object - no longer bound with arrow functions

const add = (a, b) => {
    // console.log(arguments);
    return a + b;
}
console.log(add(55, 1));


// this keyword - no longer bound

const user = {
    name: 'Andre',
    cities: [
        'Porto',
        'Lisboa',
        'Algarve'
    ],
    printPlacesLived () {
        const cityMessages = this.cities.map((city) => this.name + ' has lived in ' + city);
        return cityMessages;
    }
}
console.log(user.printPlacesLived());

// Challenge area

const multiplier = {
    //numbers - array
    //multiplyBy - single number
    // multiply - return a new array where the numbers have been multiplied
    numbers: [5, 10],
    multiplyBy: 2,
    multiply() {
        const multipliedNums = this.numbers.map((number) => this.multiplyBy * number);
        return multipliedNums;
    }
}

console.log(multiplier.multiply());