// Challenge 
class Person {
    constructor(name = 'Anonymous', age = 0) {
        this.name = name;
        this.age = age;
    }
    getGretting() {
        return `Hi. I am  ${this.name}.`;
    }
    getDescription() {
        return `${this.name} is ${this.age} year(s) old.`;
    }
}

class Student extends Person {
    constructor(name, age, major) {
        super(name, age);
        this.major = major;
    }
    hasMajor() {
        return !!this.major;
    }
    getDescription() {
        let description = super.getDescription();
        if (this.hasMajor()) {
            description += ` Their major is ${this.major}.`;
        }
        return description;
    }
}

class Traveler extends Person {
    constructor(name, age, homeLocation) {
        super(name, age);
        this.homeLocation = homeLocation;
    }
    getGretting() {
        let description = super.getGretting();
        if (this.homeLocation) {
            description += ` I'm visiting from ${this.homeLocation}`;
        }
        return description;
    }
}

const me = new Student('Marco Gomes', 18, 'Computer Science');
console.log(me.getDescription());
const other = new Student();
console.log(other.getDescription());

const traveler = new Traveler('Marcelo', 5.5);
console.log(traveler.getGretting());