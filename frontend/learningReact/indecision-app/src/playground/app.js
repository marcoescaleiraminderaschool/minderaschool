class IndecisionApp extends React.Component {
  constructor(props) {
    super(props);
    this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
    this.handlePick          = this.handlePick.bind(this);
    this.handleAddOption     = this.handleAddOption.bind(this);
    this.handleDeleteOption  = this.handleDeleteOption.bind(this);
    this.state = {
      options: []
    }
  }

  componentDidMount() {
    try {
      const json = localStorage.getItem('options');
      const optionsJSON = JSON.parse(json);
      
      if(optionsJSON) {
        this.setState(() => ({ options: optionsJSON }));
      }
    } catch(e) {
      //Do nothin
    }

  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.options.length !== this.state.options.length) {
      const json = JSON.stringify(this.state.options);
      localStorage.setItem('options', json);
    }
  }
  
  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  handleDeleteOptions() {
    this.setState(() => ({ options: [] }));
  }

  handleDeleteOption(opt) {
    this.setState((prevState) => ({
      options: prevState.options.filter((option) => option !== opt)
    }));
  } 

  handlePick() {
    const randomNum = Math.floor(Math.random() * this.state.options.length);
    const opt = this.state.options[randomNum];
    alert(opt);
  }

  handleAddOption(option) {
    if (!option) {
      return 'Enter a valid value to add item!';
    } else if (this.state.options.indexOf(option) > -1) { //Already exists
      return 'This option already exists';
    }

    this.setState( (prevState) => ({ 
      options: prevState.options.concat(option) 
    }));
  }  
  
  render() {
    const subtitle = 'Put your life in the hands of a computer';

    return (
      <div>
        <Header 
          subtitle={subtitle}
        />
        <Action 
          hasOptions={this.state.options.length > 0} 
          handlePick={this.handlePick}
        />
        <Options 
          options={this.state.options} 
          handleDeleteOptions={this.handleDeleteOptions}  
          handleDeleteOption={this.handleDeleteOption}
        />
        <AddOption 
          handleAddOption={this.handleAddOption}
        />
      </div>
    );
  }
}

const Header = (props) => {
  return (
    <div>
      <h1>{props.title}</h1>
      {props.subtitle && ( <h2>{props.subtitle}</h2> )}
    </div>
  );
}

Header.defaultProps = {
  title: 'Indecision'
};


const Action = (props) => {
  return (
    <div>
      <button 
        disabled={!props.hasOptions} 
        onClick={props.handlePick}
      >
        What should I do?
      </button>
    </div>
  );
};


const Options = (props) => {
  return (
    <div>
      <button onClick={props.handleDeleteOptions}>Remove all</button>
      {
        props.options.length === 0 && (
          <p>Please add an option to get started</p>
        )
      }
      {
        props.options.map(opt => 
          <Option 
            key={opt} 
            optionText={opt} 
            handleDeleteOption={props.handleDeleteOption} 
          />
        )
      }
    </div>
  );
}


const Option = (props) => {
  return (
    <div>
      { props.optionText }
      <button onClick={ (e) => {
          props.handleDeleteOption(props.optionText)
        }
      }>
        Remove
      </button>
    </div>
  );
}


class AddOption extends React.Component {
  constructor(props) {
    super(props);
    this.handleAddOption = this.handleAddOption.bind(this);
    this.state = {
      error: undefined
    }
  }

  handleAddOption(e) {
    e.preventDefault();

    const value = e.target.elements.optionValue.value.trim();
    const action = this.props.handleAddOption(value);

    this.setState(() => ({ error: action }))

    if(!action) {
      e.target.elements.optionValue.value = '';
    }
   }

  render() {
    return (
      <div>
        {this.state.error && (
          <p>{this.state.error}</p>
        )}
        <form onSubmit={this.handleAddOption}>
          <input type="text" name="optionValue" placeholder="Add an option..." />
          <button type="submit">Add option</button>
        </form>
      </div>
    );
  }
} 

ReactDOM.render(<IndecisionApp />, document.getElementById('app'));