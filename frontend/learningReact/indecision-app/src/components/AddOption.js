import React from 'react';

class AddOption extends React.Component {
  state = {
    error: undefined
  }

  handleAddOption = (e) => {
    e.preventDefault();

    const value = e.target.elements.optionValue.value.trim();
    const action = this.props.handleAddOption(value);

    this.setState(() => ({ error: action }));

    if(!action) {
      e.target.elements.optionValue.value = '';
    }
  };

  render() {
    return (
      <div>
        {this.state.error && (
          <p className="add-option-error">{this.state.error}</p>
        )}
        <form className="add-option" onSubmit={this.handleAddOption}>
          <input className="add-option__input" type="text" name="optionValue" placeholder="Add an option..." />
          <button className="button" type="submit">Add option</button>
        </form>
      </div>
    );
  }
} 

export default AddOption;