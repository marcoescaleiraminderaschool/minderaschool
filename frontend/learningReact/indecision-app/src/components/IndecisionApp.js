import React from 'react';

import AddOption from './AddOption';
import Options from './Options';
import Header from './Header';
import Action from './Action';
import OptionModal from './OptionModal';

class IndecisionApp extends React.Component {
  state = {
    options: [],
    selectedOption: undefined
  }

  handleSelectedOption = () => {
    this.setState( () => ({ selectedOption: undefined }) );
  }

  handleDeleteOptions = () => {
    this.setState( () => ({ options: [] }) );
  }

  handleDeleteOption = (opt) => {
    this.setState((prevState) => ({
      options: prevState.options.filter((option) => option !== opt)
    }));
  } 

  handlePick = () => {
    const randomNum = Math.floor(Math.random() * this.state.options.length);
    const opt = this.state.options[randomNum];
    this.setState( () => ({ selectedOption: opt }) );
  }

  handleAddOption = (option) => {
    if (!option) {
      return 'Enter a valid value to add item!';
    } else if (this.state.options.indexOf(option) > -1) { //Already exists
      return 'This option already exists';
    }

    this.setState( (prevState) => ({ 
      options: prevState.options.concat(option) 
    }));
  }  
  

  componentDidMount() {
    try {
      const json = localStorage.getItem('options');
      const optionsJSON = JSON.parse(json);
      
      if(optionsJSON) {
        this.setState(() => ({ options: optionsJSON }));
      }
    } catch(e) {
      //Do nothin
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.options.length !== this.state.options.length) {
      const json = JSON.stringify(this.state.options);
      localStorage.setItem('options', json);
    }
  }
  
  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  render() {
    const subtitle = 'Put your life in the hands of a computer';

    return (
      <div>
        <Header 
          subtitle={subtitle}
        />
        <div className="container">
          <Action 
            hasOptions={this.state.options.length > 0} 
            handlePick={this.handlePick}
          />
          <div className="widget">
            <Options 
              options={this.state.options} 
              handleDeleteOptions={this.handleDeleteOptions}  
              handleDeleteOption={this.handleDeleteOption}
            />
            <AddOption 
              handleAddOption={this.handleAddOption}
            />
          </div>
        </div>
        <OptionModal 
          selectedOption={this.state.selectedOption}
          handleSelectedOption={this.handleSelectedOption}
        />
      </div>
    );
  }
}

export default IndecisionApp;