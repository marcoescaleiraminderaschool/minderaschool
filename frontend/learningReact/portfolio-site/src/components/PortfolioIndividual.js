import React from 'react';

const PortfolioIndividual = props => (
  <div>
    <p>
      Individual portfolio with the id of: <b>{props.match.params.id}</b>
    </p>
  </div>
);

export default PortfolioIndividual;
