import React from 'react';
import { Link } from 'react-router-dom';

const Portfolio = () => (
  <div>
    <p>Portfolio page showing</p>
    <p>Checkout my stuff</p>
    <Link to="/portfolio/1">One</Link>
    <Link to="/portfolio/2">Two</Link>
  </div>
);

export default Portfolio;
